//
//  SharedData.swift
//  Rent Cars
//
//  Created by Engr.aqadar@gmail.com on 06/06/2018.
//  Copyright © 2018 Engr.aqadar@gmail.com. All rights reserved.
//

import Foundation
import UIKit

class SharedData  {
    static let SharedInfo = SharedData()
    
    var globalEditedImage = UIImage()
    var blurIntensity: Float = 0.0
    var isFiltered = false
    var isBlurred = false
    var isEditingStarted = true
    var isEditable = true
    var isBackgroundColorChanged = true
    var backgroundColor = UIColor.white
    var filterUsed = BPFilter.none
    var selectedIndexPathForFilters = IndexPath(item: 0, section: 0)
    var enteredTxt = ""
    var isTxtEnterd = false
    var updateTextImageLayer = false
    var isCroped = false
    var trimmedAndCroppedVideoUrl:URL!
    var croppedTextWatermarkImage: UIImage? = nil
}
