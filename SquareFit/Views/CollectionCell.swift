//
//  CollectionCell.swift
//  SquareFit
//
//  Created by Qaiser Butt on 9/4/19.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit

class VideoCanvasCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    
}

class FontCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    
}

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lbl: UILabel!
    
}
