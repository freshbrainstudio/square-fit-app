//
//  CanvasVC
//  SquareFit
//
//  Created by Qaiser Butt on 9/4/19.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit
import Foundation
import AVKit
import PryntTrimmerView
import MBProgressHUD

enum BPAspectRatioType:Int {
    case original = 0, square, ratio34, ratio43, ratio64, ratio46, ratio76, ratio67, ratio108, ratio810, ratio169, ratio916
}

protocol CanvasCropedProtocole
{
    func pictureCroped(pictData: UIImage)
}

class CanvasVC: IGRPhotoTweakViewController{

    var textWatermarkImg: UIImage? = nil
    var cropImgDelegate : CanvasCropedProtocole? = nil
//    var selectedIndexPath: IndexPath?
    var mediaType = MediaType.photo
    var videoThumbnail: UIImage!
    var videoUrl:URL!
    var cropRect:CGRect!
    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    
    var playbackRate: Float = 1.0
    var timeSpent = 0.0
    var startTime = 0.0
    var stopTime = 0.0
    var videoAsset:AVAsset!
    

    @IBOutlet weak var videoThumbnailView: AKImageCropperView!
    @IBOutlet weak var videoSection: UIView!
    @IBOutlet weak var videoEditerView: AKImageCropperView!
    @IBOutlet weak var trimmerView: TrimmerView!
    @IBOutlet weak var editorView: UIView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var trimView: UIView!
    @IBOutlet weak var hideEditorView: UIView!
    
    
    private var rotationAngle = CGFloat(0)
    private var myImg = UIImage()
    private var firstLaunch: Bool = true
    private var previousRatio: Int = BPAspectRatioType.original.rawValue
    private var bpAspectOptions: [BPAspectRatioType] = [.original, .square, .ratio34, .ratio43, .ratio64, .ratio46, .ratio76, .ratio67, .ratio108, .ratio810, .ratio169, .ratio916]
    
    private var bpAspectRatioTitles: [String] = ["Original", "Square", "3:4", "4:3", "6:4", "4:6", "7:6", "6:7", "10:8", "8:10", "16:9", "9:16"]

    var imgArr = [UIImage(named: "1-1"),UIImage(named: "original"),UIImage(named: "custom"),UIImage(named: "9-16"),UIImage(named: "1-1"),UIImage(named: "4-5"), UIImage(named: "16-9") , UIImage(named: "2-3")]
    
    var nameArr = ["Trim","Original","Custom", "9:16", "1:1", "4:5", "16:9", "2:3"]
    var photoCanvas = ["Original","Custom", "9:16", "1:1", "4:5", "16:9", "2:3"]
    var photoImages = [UIImage(named: "original"),UIImage(named: "custom"),UIImage(named: "9-16"),UIImage(named: "1-1"),UIImage(named: "4-5"), UIImage(named: "16-9") , UIImage(named: "2-3")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mediaType == .photo {
            editorView.isHidden = false
            videoSection.isHidden = true
            let indexPath = IndexPath(item: 0, section: 0)
//            selectedIndexPath = indexPath
            collection.selectItem(at: indexPath, animated: true, scrollPosition: [.centeredVertically, .centeredHorizontally])
        } else if mediaType == .video {
            //self.image
            editorView.isHidden = true
            hideEditorView.isHidden = false
            videoSection.isHidden = true
            let indexPath = IndexPath(item: 1, section: 0)
//            selectedIndexPath = indexPath
            collection.selectItem(at: indexPath, animated: true, scrollPosition: [.centeredVertically, .centeredHorizontally])
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if mediaType == .video{
            videoEditerView.backgroundColor = .black
            videoThumbnailView.backgroundColor = .black
            videoAsset = AVAsset.init(url: videoUrl)
            videoThumbnailView.image = videoThumbnail
            videoThumbnailView.showOverlayView()
            videoThumbnailView.reset()
            videoThumbnailView.delegate = self
            hideVideoTrimView()
            videoThumbnailView.overlayView?.configuraiton.minCropRectSize = CGSize.init(width: self.videoEditerView.bounds.size.width, height: self.videoEditerView.bounds.size.width)
            videoThumbnailView.layoutSubviews()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.trimmerView.layer.cornerRadius = 3
                self.trimmerView.layer.masksToBounds = true
                self.trimmerView.minDuration = 2
                self.trimmerView.asset = self.videoAsset
                self.trimmerView.delegate = self
                self.addVideoPlayer(with: self.videoAsset, playerView: self.videoEditerView)
                self.startTime = CMTimeGetSeconds(self.trimmerView.startTime!)
                self.stopTime  = CMTimeGetSeconds(self.trimmerView.endTime!)
            }
        }
    }
    func openPreviewScreen(_ videoURL:URL) -> Void {
        let player = AVPlayer(url: videoURL)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        present(playerController, animated: true, completion: {
            player.play()
        })
    }

    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        var videoComposition: AVMutableVideoComposition? = nil
        
        /*if SharedData.SharedInfo.isBlurred {
            let avAsset = AVAsset(url: videoUrl)
            let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: {
                request in
                var image = request.sourceImage
                let blurIntensity = SharedData.SharedInfo.blurIntensity
                
                image = Utils.filterWithGaussianBlur(radius: blurIntensity, imgObject: image, isVideoProcessing: true) as! CIImage
                
                if SharedData.SharedInfo.isFiltered {
                    let bpfilter = SharedData.SharedInfo.filterUsed
                    
                    if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                        image = img
                    }
                }
                if SharedData.SharedInfo.isTxtEnterd {
                    if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                        let wCIImage = CIImage(cgImage: wcg)
                        image = wCIImage.composited(over: image)
                    }
                }
                
                request.finish(with: image, context: nil)
            })
            
            videoComposition = vidComp
        } else*/ if SharedData.SharedInfo.isFiltered {
            let avAsset = AVAsset(url: videoUrl)
            let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: {
                request in
                var image = request.sourceImage
                let bpfilter = SharedData.SharedInfo.filterUsed
                if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                    image = img
                }
                if SharedData.SharedInfo.isTxtEnterd {
                    if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                        let wCIImage = CIImage(cgImage: wcg)
                        image = wCIImage.composited(over: image)
                    }
                }
                request.finish(with: image, context: nil)
            })
            
            videoComposition = vidComp
            
        } else if SharedData.SharedInfo.isTxtEnterd {
            let avAsset = AVAsset(url: videoUrl)
            let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: {
                request in
                var image = request.sourceImage
                
                if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                    let wCIImage = CIImage(cgImage: wcg)
                    image = wCIImage.composited(over: image)
                }
                
                request.finish(with: image, context: nil)
            })
            
            videoComposition = vidComp
        }
        
        if let composition = videoComposition {
            let playerItem = AVPlayerItem(asset: asset)
            playerItem.videoComposition = composition
            player = AVPlayer(playerItem: playerItem)

            NotificationCenter.default.addObserver(self, selector: #selector(CanvasVC.itemDidFinishPlaying(_:)),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

            let layer: AVPlayerLayer = AVPlayerLayer(player: player)
            layer.frame = videoEditerView.bounds
            layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerView.layer.addSublayer(layer)
            
        } else {
            let playerItem = AVPlayerItem(asset: asset)
            player = AVPlayer(playerItem: playerItem)

            NotificationCenter.default.addObserver(self, selector: #selector(CanvasVC.itemDidFinishPlaying(_:)),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

            let layer: AVPlayerLayer = AVPlayerLayer(player: player)
            layer.frame = videoEditerView.bounds
            layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerView.layer.addSublayer(layer)
        }
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }
    }
    // MARK: - Trim

    func TrimVideo(asset: AVAsset, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil)
    {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

//        let asset = AVAsset(url: sourceURL)
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        print("video length: \(length) seconds")

        var outputURL = documentDirectory.appendingPathComponent("output")
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("trimmed.mp4")
        }catch let error {
            print(error)
        }

        //Remove existing file
        try? fileManager.removeItem(at: outputURL)

        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4

        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                    end: CMTime(seconds: endTime, preferredTimescale: 1000))

        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                print("exported at \(outputURL)")
                completion?(outputURL)
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func getVideoThumbnailWithEffects(from asset:AVAsset,at Duration:CMTime) -> UIImage {
        var videoThumbnail: UIImage? = getVideoThumbnail(from: asset, at: Duration)
        
        /*if SharedData.SharedInfo.isBlurred {
            if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
                var image = CIImage(cgImage: vcgImage)
                let blurIntensity = SharedData.SharedInfo.blurIntensity
                
                image = Utils.filterWithGaussianBlur(radius: blurIntensity, imgObject: image, isVideoProcessing: true) as! CIImage
                
                if SharedData.SharedInfo.isFiltered {
                    let bpfilter = SharedData.SharedInfo.filterUsed
                    
                    if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                        image = img
                    }
                }
                if SharedData.SharedInfo.isTxtEnterd {
                    if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                        let wCIImage = CIImage(cgImage: wcg)
                        image = wCIImage.composited(over: image)
                    }
                }

                videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
                                                        imageOrientation: videoThumbnail?.imageOrientation ?? .up)
            }
        } else*/ if SharedData.SharedInfo.isFiltered {
            if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
                var image = CIImage(cgImage: vcgImage)
                let bpfilter = SharedData.SharedInfo.filterUsed
                
                if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                    image = img
                }
                if SharedData.SharedInfo.isTxtEnterd {
                    if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                        let wCIImage = CIImage(cgImage: wcg)
                        image = wCIImage.composited(over: image)
                    }
                }

                videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
                                                        imageOrientation: videoThumbnail?.imageOrientation ?? .up)
            }
        } else if SharedData.SharedInfo.isTxtEnterd {
            if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
                var image = CIImage(cgImage: vcgImage)
                if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                    let wCIImage = CIImage(cgImage: wcg)
                    image = wCIImage.composited(over: image)
                }

                videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
                                                        imageOrientation: videoThumbnail?.imageOrientation ?? .up)
            }
        }
        
        return videoThumbnail ?? UIImage.init()
    }
    
    // MARK: - Get Video Thumbnail

    func getVideoThumbnail(from asset:AVAsset,at Duration:CMTime) -> UIImage {
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let timestamp = Duration
        let thumbnail: UIImage?
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            thumbnail = UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            thumbnail = nil
            print("Image generation failed with error \(error)")
        }
        return thumbnail ?? UIImage.init()
    }
    
    
    // MARK: - IBActions

    @IBAction func actionBack(_ sender: UIButton) {
        self.popVC()
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        if mediaType == .video {
            textWatermarkImg = textWatermarkImg?.ic_imageInRect(videoThumbnailView.scrollView.scaledVisibleRect)?.ic_rotateByAngle(0.0)
            FinalCrop()
        } else {
            let imageToProcess = self.image
            var transform = CGAffineTransform.identity
            // translate
            let translation: CGPoint = self.photoView.photoTranslation
            transform = transform.translatedBy(x: translation.x, y: translation.y)
            // rotate
            transform = transform.rotated(by: self.photoView.radians)
            // scale
            let t: CGAffineTransform = self.photoView.photoContentView.transform
            let xScale: CGFloat = sqrt(t.a * t.a + t.c * t.c)
            let yScale: CGFloat = sqrt(t.b * t.b + t.d * t.d)
                
            transform = transform.scaledBy(x: xScale, y: yScale)
                
            if let fixedImage = imageToProcess?.cgImageWithFixedOrientation() {
                let imageRef = fixedImage.transformedImage(transform,
                                                           zoomScale: self.photoView.scrollView.zoomScale,
                                                           sourceSize: imageToProcess?.size ?? CGSize(width: 100, height: 100),
                                                           cropSize: self.photoView.cropView.frame.size,
                                                           imageViewSize: self.photoView.photoContentView.bounds.size)
                let image = UIImage(cgImage: imageRef)
                self.delegate?.photoTweaksController(self, didFinishWithCroppedImage: image)
            }
        }
    }
    
    func FinalTrim(){
        let videoAsset:AVAsset!
        videoAsset = AVAsset.init(url: videoUrl)
        TrimVideo(asset: videoAsset, startTime: startTime, endTime: stopTime) { (url) in
            print("Trimmed",url)
            DispatchQueue.main.async {
                self.openPreviewScreen(url)
            }
        }
    }
    
    func FinalCrop() {
        showHUD(progressLabel: "Processing")
        
        guard let image = videoThumbnailView.croppedImage else {
            return
        }
        let videoAsset:AVAsset!
        videoAsset = AVAsset.init(url: videoUrl)
        
        let path = NSTemporaryDirectory().appending("demo.mov")
        let exportURL = URL.init(fileURLWithPath: path)
        TrimAndCropVideo(videoAsset: videoAsset, startTime: startTime, endTime: stopTime, inputURL: exportURL, frame: CGRect.init(origin: CGPoint.zero, size: image.size)) { [weak self] url in
            SharedData.SharedInfo.isCroped = true
            SharedData.SharedInfo.isEditingStarted = true
            SharedData.SharedInfo.trimmedAndCroppedVideoUrl = url
            SharedData.SharedInfo.croppedTextWatermarkImage = self?.textWatermarkImg
            
            DispatchQueue.main.async {
                self?.popVC()
                self?.dismissHUD(isAnimated: true)
                //self?.openPreviewScreen(url!)
            }
        }
    }
    
    @IBAction func actionRotateLeft(_ sender: Any) {
        if mediaType == .video{
            
        }
        
        rotationAngle = rotationAngle - CGFloat(Double.pi / 2.0)
        self.photoView.changeAngle(radians: rotationAngle)
        
    }
    
    @IBAction func actionRotateRight(_ sender: Any) {
        if mediaType == .video{
            
        }
        rotationAngle = rotationAngle + CGFloat(Double.pi / 2.0)
        self.photoView.changeAngle(radians: rotationAngle)
    }
    
    @IBAction func actionFlipHorizental(_ sender: Any) {
        if mediaType == .video{
            
        }
        self.photoView.flipViewHorizontally()
    }
    
    @IBAction func actionFlipVertical(_ sender: Any) {
        if mediaType == .video{
            
        }
        self.photoView.flipViewVerticaly()
    }
}

// MARK:- CollectionView

extension CanvasVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mediaType == .photo {
            return photoCanvas.count
        }
        return self.nameArr.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        let backgroundSelectionView = UIView()
        backgroundSelectionView.bounds = cell.img.frame
        backgroundSelectionView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundSelectionView
        if mediaType == .photo{
            cell.lbl.text = self.photoCanvas[indexPath.item]
            cell.img.image = self.photoImages[indexPath.item]
            
        }else{
            cell.lbl.text = self.nameArr[indexPath.item]
            cell.img.image = self.imgArr[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)!")
        //let cell = collectionView.cellForItem(at: indexPath) as! CollectionCell

        //cell.img.backgroundColor = .white

//        self.selectedIndexPath = indexPath
        if mediaType == .video{
            if indexPath.item == 0  {
                editorView.isUserInteractionEnabled = false
                //editorView.isHidden = true
                videoSection.isHidden = false
                videoThumbnailView.isHidden = true
                trimView.isHidden = false
            }else if indexPath.item == 1  {  // Original
                hideVideoTrimView()
                videoThumbnailView.image = videoThumbnail
                videoThumbnailView.reset()
                videoThumbnailView.hideOverlayView()
                
                //videoThumbnailView.showOverlayView()
                videoThumbnailView.overlayView?.configuraiton.minCropRectSize = CGSize.init(width: self.videoEditerView.bounds.size.width, height: self.videoEditerView.bounds.size.width)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 2 {  // Custom
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.overlayView?.configuraiton.minCropRectSize = CGSize.init(width: self.videoEditerView.bounds.size.width, height: self.videoEditerView.bounds.size.width)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 3 {  // 9:16
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.changeToCustomFrame(aspect:9.0/16.0)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 4 {  // 1:1
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.changeToCustomFrame(aspect:1.0/1.0)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 5 {  // 4:5
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.changeToCustomFrame(aspect:4.0/5.0)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 6 {  // 16:9
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.changeToCustomFrame(aspect:16.0/9.0)
                videoThumbnailView.layoutSubviews()
            }else if indexPath.item == 7 {  // 2:3
                hideVideoTrimView()
                videoThumbnailView.showOverlayView()
                videoThumbnailView.changeToCustomFrame(aspect:2.0/3.0)
                videoThumbnailView.layoutSubviews()
            }
        }else{
            if indexPath.item == 0  {  // Original
                resetAspectRect()
            }else if indexPath.item == 1 {  // Custom
                
            }else if indexPath.item == 2 {  // 9:16
                
                setCropAspectRect(aspect: "9:16")

            }else if indexPath.item == 3 {  // 1:1
                
                setCropAspectRect(aspect: "1:1")

            }else if indexPath.item == 4 {  // 4:5
                
                setCropAspectRect(aspect: "4:5")

            }else if indexPath.item == 5 {  // 16:9
                
                setCropAspectRect(aspect: "16:9")

            }else if indexPath.item == 6 {  // 2:3
                
                setCropAspectRect(aspect: "2:3")
            }
        }
        
        
    }
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as? CollectionCell
//        cell?.img.backgroundColor = .clear
////        self.selectedIndexPath = nil
//    }
    func hideVideoTrimView(){
        editorView.isUserInteractionEnabled = true
        //editorView.isHidden = false
        videoSection.isHidden = false
        videoThumbnailView.isHidden = false
        trimView.isHidden = true
    }
}

extension CanvasVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/8.0
    
        let yourHeight = collectionView.bounds.width/5.5
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension CanvasVC: IGRPhotoTweakViewControllerDelegate {
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        print(croppedImage)
        SharedData.SharedInfo.globalEditedImage = croppedImage
        SharedData.SharedInfo.isEditable = true
        SharedData.SharedInfo.isEditingStarted = true
        //self.saveToLibrary(image: croppedImage)
        _ = controller.navigationController?.popViewController(animated: false)

    }
    
    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
        _ = controller.navigationController?.popViewController(animated: false)
    }
}



// MARK:- Video Croping Functions
extension CanvasVC{
    
    func TrimAndCropVideo(videoAsset:AVAsset,startTime: Double, endTime: Double,inputURL: URL , frame: CGRect , completion: @escaping (_ outputURL : URL?) -> ()) {
        
        let composition = AVMutableComposition()
        composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        guard let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else {
            return
        }
 
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = frame.size
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration)
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        
        
        let renderSize = frame.size
        let renderScale = renderSize.width / cropRect.width
        let offset = CGPoint(x: -cropRect.origin.x, y: -cropRect.origin.y)
        let rotation = atan2(clipVideoTrack.preferredTransform.b, clipVideoTrack.preferredTransform.a)
        var rotationOffset = CGPoint(x: 0, y: 0)
        
        if clipVideoTrack.preferredTransform.b == -1.0 {
            rotationOffset.y = clipVideoTrack.naturalSize.width
        } else if clipVideoTrack.preferredTransform.c == -1.0 {
            rotationOffset.x = clipVideoTrack.naturalSize.height
        } else if clipVideoTrack.preferredTransform.a == -1.0 {
            rotationOffset.x = clipVideoTrack.naturalSize.width
            rotationOffset.y = clipVideoTrack.naturalSize.height
        }
        
        var transform = CGAffineTransform.identity
        transform = transform.scaledBy(x: renderScale, y: renderScale)
        transform = transform.translatedBy(x: offset.x + rotationOffset.x, y: offset.y + rotationOffset.y)
        transform = transform.rotated(by: rotation)
        
        transformer.setTransform(transform, at: CMTime.zero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
                
        FileManager.default.removeItemIfExisted(inputURL)
        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
        end: CMTime(seconds: endTime, preferredTimescale: 1000))

        let exporter = AVAssetExportSession.init(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = inputURL
        exporter?.timeRange = timeRange
        exporter?.outputFileType = .mov
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.videoComposition = videoComposition
        exporter?.exportAsynchronously {
            switch exporter?.status {
               case .completed:
                completion(exporter?.outputURL)
               default:
                   completion(nil)
            }
        }
    }
}
extension CanvasVC: AKImageCropperViewDelegate {
    
    func imageCropperViewDidChangeCropRect(view: AKImageCropperView, cropRect rect: CGRect) {
        cropRect = rect
             print("New crop rectangle: \(rect)")
    }
    
}

extension FileManager {
    func removeItemIfExisted(_ url:URL) -> Void {
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(atPath: url.path)
            }
            catch {
                print("Failed to delete file")
            }
        }
    }
}

// MARK:- TrimmerView Delegates

extension CanvasVC: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        startTime = CMTimeGetSeconds(trimmerView.startTime!)
        stopTime  = CMTimeGetSeconds(trimmerView.endTime!)
        videoThumbnail = getVideoThumbnailWithEffects(from: videoAsset, at: trimmerView.startTime ?? CMTime(seconds: 1, preferredTimescale: 60))
        videoThumbnailView.image = getVideoThumbnailWithEffects(from: videoAsset, at: trimmerView.startTime ?? CMTime(seconds: 1, preferredTimescale: 60))
        startPlaybackTimeChecker()
    }
    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        player?.pause()
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
    func startPlaybackTimeChecker() {

        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(CanvasVC.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }

    func stopPlaybackTimeChecker() {

        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }

    @objc func onPlaybackTimeChecker() {

        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }

        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)

        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }
    }
}

// MARK:- Testing

extension CanvasVC{
       func mirrorVideo(completion: @escaping (_ outputURL : URL?) -> ())
    {
        //let videoAsset: AVAsset = AVAsset( url: inputURL )
        let clipVideoTrack = videoAsset.tracks( withMediaType: AVMediaType.video ).first! as AVAssetTrack

        let composition = AVMutableComposition()
        composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID())

        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = CGSize(width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)

        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)

        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMakeWithSeconds(60, preferredTimescale: 30))
        var transform:CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        transform = transform.translatedBy(x: -clipVideoTrack.naturalSize.width, y: 0.0)
        transform = transform.rotated(by: CGFloat(Double.pi/2))
        transform = transform.translatedBy(x: 0.0, y: -clipVideoTrack.naturalSize.width)

        transformer.setTransform(transform, at: CMTime.zero)

        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]

        // Export

        let exportSession = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPreset640x480)!
        let path = NSTemporaryDirectory().appending("demo.mp4")
        let exportURL = URL.init(fileURLWithPath: path)
        
        
        let croppedOutputFileUrl = exportURL
        exportSession.outputURL = croppedOutputFileUrl
        exportSession.outputFileType = AVFileType.mp4
        exportSession.videoComposition = videoComposition
        exportSession.exportAsynchronously {
            if exportSession.status == .completed {
                DispatchQueue.main.async(execute: {
                    print(exportSession.outputURL)
                    completion(croppedOutputFileUrl)
                })
                return
            } else if exportSession.status == .failed {
                print("Export failed - \(String(describing: exportSession.error))")
            }

            completion(nil)
            return
        }
    }
    
    func cropVideo(){
            let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first! as AVAssetTrack

            let composition = AVMutableComposition()
            composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID())

            let videoComposition = AVMutableVideoComposition()

            videoComposition.renderSize = CGSize(width: clipVideoTrack.naturalSize.height, height: clipVideoTrack.naturalSize.width)
            videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
//        let instruction = AVMutableVideoCompositionInstruction()
//        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMakeWithSeconds(60, preferredTimescale: 30))
//        var transform:CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        transform = transform.translatedBy(x: -clipVideoTrack.naturalSize.width, y: 0.0)
//        transform = transform.rotated(by: CGFloat(Double.pi/2))
//        transform = transform.translatedBy(x: 0.0, y: -clipVideoTrack.naturalSize.width)

            let instruction = AVMutableVideoCompositionInstruction()

            instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMakeWithSeconds(180, preferredTimescale: 30))

            // rotate to portrait
            let transformer:AVMutableVideoCompositionLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
//            let t1 = CGAffineTransform(translationX: 720, y: 0);
//            let t2 = t1.rotated(by: CGFloat(CGFloat.pi/2));
        
//        var transform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//        transform = transform.rotated(by: CGFloat(Double.pi/2))
//        self.videoInputWriter.transform = transform


        var transform:CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        transform = transform.translatedBy(x: -clipVideoTrack.naturalSize.width, y: 0.0)
        transform = transform.rotated(by: CGFloat(Double.pi/2))
        transform = transform.translatedBy(x: 0.0, y: -clipVideoTrack.naturalSize.width)
        
//        var transform:CGAffineTransform = CGAffineTransform(scaleX: 1.0, y: -1.0)
//        transform = transform.translatedBy(x: 0.0, y: -clipVideoTrack.naturalSize.height)
//        transform = transform.rotated(by: CGFloat(Double.pi/2))
//        transform = transform.translatedBy(x: -clipVideoTrack.naturalSize.h, y:0.0 )

        transformer.setTransform(transform, at: CMTime.zero)
            //transformer.setTransform(t2, at: CMTime.zero)
            instruction.layerInstructions = [transformer]
            videoComposition.instructions = [instruction]

            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            let cropUniqueId = NSUUID().uuidString
            let outputPath = "\(documentsPath)/\(cropUniqueId).mov"
            let relativePath = "\(cropUniqueId).mov"
            let relativeURL = URL(fileURLWithPath: relativePath)
            let outputUrl = URL(fileURLWithPath: outputPath, relativeTo: relativeURL)
            let exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPreset1280x720)!
            exporter.videoComposition = videoComposition
            exporter.outputURL = outputUrl
            exporter.outputFileType = AVFileType.mov
            exporter.shouldOptimizeForNetworkUse = true

            exporter.exportAsynchronously(completionHandler: { () -> Void in
                DispatchQueue.main.async(execute: {
                    self.openPreviewScreen(exporter.outputURL!)
                    //self.handleExportCompletion(exporter, removedPath)
                })
            })
        }
}
