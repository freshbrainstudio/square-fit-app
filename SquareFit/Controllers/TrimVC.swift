//
//  TrimVC.swift
//  SquareFit
//
//  Created by Bilal Nawaz on 23/10/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit
import AVKit
import PryntTrimmerView

class TrimVC: UIViewController {

    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    var videoThumbnail: UIImage!
    var videoUrl:URL!

    var videoAsset:AVAsset!
    var playbackRate: Float = 1.0
    var timeSpent = 0.0
    var startTime = 0.0
    var stopTime = 0.0
    
    var isTrimmed = false
    
    @IBOutlet weak var trimmerView: TrimmerView!
    @IBOutlet weak var videoEditerView: AKImageCropperView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoAsset = AVAsset.init(url: self.videoUrl)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.videoAsset = AVAsset.init(url: self.videoUrl)
            self.trimmerView.layer.cornerRadius = 3
            self.trimmerView.layer.masksToBounds = true
            self.trimmerView.minDuration = 2
            self.trimmerView.asset = self.videoAsset
            self.trimmerView.delegate = self
            self.addVideoPlayer(with: self.videoAsset, playerView: self.videoEditerView)
            self.startTime = CMTimeGetSeconds(self.trimmerView.startTime!)
            self.stopTime  = CMTimeGetSeconds(self.trimmerView.endTime!)
        }

    }
    
    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)

        NotificationCenter.default.addObserver(self, selector: #selector(CanvasVC.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.frame = videoEditerView.bounds
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.addSublayer(layer)
        player?.play()
    }
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        popToRootVC()
//        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func goToMainVC(_ sender: Any) {
        if isTrimmed{
            FinalTrim()
        }else{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC
            vc?.myImg = nil
            vc?.videoURL = videoUrl
            vc?.mediaType = .video
            SharedData.SharedInfo.isEditable = false
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    func FinalTrim(){
        showHUD(progressLabel: "Trimming")
        player?.pause()
        let videoAsset:AVAsset!
        videoAsset = AVAsset.init(url: videoUrl)
        TrimVideo(asset: videoAsset, startTime: startTime, endTime: stopTime) { (url) in
            print("Trimmed",url)
            DispatchQueue.main.async {
                self.dismissHUD(isAnimated: true)
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC
                vc?.myImg = nil
                vc?.videoURL = url
                vc?.mediaType = .video
                SharedData.SharedInfo.isEditable = false
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
    
        // MARK: - Trim

        func TrimVideo(asset: AVAsset, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil)
        {
            let fileManager = FileManager.default
            let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

    //        let asset = AVAsset(url: sourceURL)
            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
            print("video length: \(length) seconds")

            var outputURL = documentDirectory.appendingPathComponent("output")
            do {
                try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                outputURL = outputURL.appendingPathComponent("trimmed.mp4")
            }catch let error {
                print(error)
            }

            //Remove existing file
            try? fileManager.removeItem(at: outputURL)

            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4

            let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                        end: CMTime(seconds: endTime, preferredTimescale: 1000))

            exportSession.timeRange = timeRange
            exportSession.exportAsynchronously {
                switch exportSession.status {
                case .completed:
                    print("exported at \(outputURL)")
                    completion?(outputURL)
                case .failed:
                    print("failed \(exportSession.error.debugDescription)")
                case .cancelled:
                    print("cancelled \(exportSession.error.debugDescription)")
                default: break
                }
            }
        }
    func getVideoThumbnail(from asset:AVAsset,at Duration:CMTime) -> UIImage {
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let timestamp = Duration
        let thumbnail: UIImage?
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            thumbnail = UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            thumbnail = nil
            print("Image generation failed with error \(error)")
        }
        return thumbnail ?? UIImage.init()
    }
    
}
// MARK:- TrimmerView Delegates

extension TrimVC: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        startTime = CMTimeGetSeconds(trimmerView.startTime!)
        stopTime  = CMTimeGetSeconds(trimmerView.endTime!)
        videoThumbnail = getVideoThumbnail(from: videoAsset, at: trimmerView.startTime ?? CMTime(seconds: 1, preferredTimescale: 60))
        videoEditerView.image = getVideoThumbnail(from: videoAsset, at: trimmerView.startTime ?? CMTime(seconds: 1, preferredTimescale: 60))
        startPlaybackTimeChecker()
    }
    func didChangePositionBar(_ playerTime: CMTime) {
        isTrimmed = true
        stopPlaybackTimeChecker()
        player?.play()
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
    func startPlaybackTimeChecker() {

        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(TrimVC.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }

    func stopPlaybackTimeChecker() {

        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }

    @objc func onPlaybackTimeChecker() {

        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }

        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)

        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }
    }
}
