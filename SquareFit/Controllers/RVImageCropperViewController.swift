//
//  RVImageCropperViewController.swift
//  RADVHS
//
//  Created by Polina Lobanova on 30/01/2019.
//  Copyright © 2019 Polina Lobanova. All rights reserved.
//

import UIKit
import AVFoundation

class RVimageCropperViewController: UIViewController {
    
    @IBOutlet weak var cropView: AKImageCropperView!
    @IBOutlet weak var squareButton: UIButton!
    
    @IBOutlet weak var portraitButton: UIButton!
    
    var image: UIImage!
    var videeAsset: AVAsset!
    
    var cropRect:CGRect!
    
    override func viewDidLoad() {

        navigationController?.isNavigationBarHidden = true
        cropView.image = image
        cropView.showOverlayView()
        cropView.delegate = self
        cropView.overlayView?.configuraiton.minCropRectSize = CGSize.init(width: self.cropView.bounds.size.width - 40.0, height: self.cropView.bounds.size.width - 40.0)
        
        squareButton.alignTextBelow()
        portraitButton.alignTextBelow()
        
        cropView.squareMode = true
        cropView.changeToSquareFrame()
        portraitButton.tintColor = UIColor.gray
        portraitButton.setTitleColor(UIColor.gray, for: .normal)
    }
    
    
    
    //MARK: Actions
    
    @IBAction func squareButtonTapped(_ sender: Any) {
        cropView.squareMode = true
        cropView.changeToSquareFrame()
        portraitButton.tintColor = UIColor.gray
        portraitButton.setTitleColor(UIColor.gray, for: .normal)
        squareButton.tintColor = UIColor.white
        squareButton.setTitleColor(UIColor.white, for: .normal)
        cropView.layoutSubviews()
    }
    
    @IBAction func portraitButtonTapped(_ sender: Any) {
        cropView.squareMode = false
        cropView.changeToPortraitFrame()
        squareButton.tintColor = UIColor.gray
        squareButton.setTitleColor(UIColor.gray, for: .normal)
        portraitButton.tintColor = UIColor.white
        portraitButton.setTitleColor(UIColor.white, for: .normal)
        cropView.layoutSubviews()
    }
    
    @IBAction func cancelBUttonTapped(_ sender: Any) {
        
        guard !cropView.isEdited else {
            
            let alertController = UIAlertController(title: "Warning!", message:
                "All changes will be lost.", preferredStyle: UIAlertController.Style.alert)
            
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.cancel, handler: { _ in
                
                _ = self.navigationController?.popViewController(animated: true)
            }))
            
            alertController.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
            
            present(alertController, animated: true, completion: nil)
            return
        }
        
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        guard let image = cropView.croppedImage else {
            return
        }
        if (videeAsset == nil) {
            let importController = UIStoryboard(name: "Import", bundle: nil).instantiateInitialViewController() as! RVImportViewController
            UserDefaults.standard.set(false, forKey: "cropMode")
            importController.viewModel = RVImportViewModel()
            importController.viewModel.delegate = importController
            importController.videoURL = nil
            importController.inputImage = image
            importController.imageMode = true
            importController.viewModel.isPhotoMode = true
            //self.present(importController, animated: false, completion: nil)
            self.navigationController?.pushViewController(importController, animated: true)
            
        }else {
            SKActivityIndicator.show()
            SKActivityIndicator.spinnerStyle(.spinningCircle)
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            let videoURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent( NSUUID().uuidString).appendingPathExtension("mov")
            cropVideo(inputURL: videoURL, frame: CGRect.init(origin: CGPoint.zero, size: image.size)) { [weak self] url in
                DispatchQueue.main.async {
                    UIApplication.shared.endIgnoringInteractionEvents()
                    SKActivityIndicator.dismiss()
                    
                    let importController = UIStoryboard(name: "Import", bundle: nil).instantiateInitialViewController() as! RVImportViewController
                    if (self!.cropRect.size.width == self!.cropRect.size.height) {
                         UserDefaults.standard.set(true, forKey: "cropMode")
                    }else { UserDefaults.standard.set(false, forKey: "cropMode") }
                    importController.viewModel = RVImportViewModel()
                    importController.viewModel.delegate = importController
                    importController.videoURL = url
                    importController.inputImage = nil
                    importController.imageMode = false
                    importController.viewModel.isPhotoMode = false
                    
                    self!.navigationController?.pushViewController(importController, animated: true)
                }
            }
        }
        
    }
    
    
    //MARK: Video render
    
    
    func cropVideo(inputURL: URL , frame: CGRect , completion: @escaping (_ outputURL : URL?) -> ()) {
        let composition = AVMutableComposition()
        composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let clipVideoTrack = videeAsset.tracks( withMediaType: AVMediaType.video ).first! as AVAssetTrack
        
        let videoComposition = AVMutableVideoComposition()
        
        videoComposition.renderSize = frame.size
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: videeAsset.duration)
        
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        /*var t1 : CGAffineTransform
        let transformedVideoSize = clipVideoTrack.naturalSize.applying(clipVideoTrack.preferredTransform)
        let videoIsPortrait = abs(transformedVideoSize.width) < abs(transformedVideoSize.height)
        if (videoIsPortrait) {
             t1 = CGAffineTransform(translationX: cropRect.size.width , y: -cropRect.origin.y)
             t1 = t1.rotated(by: .pi / 2)
        }else {
            t1 = CGAffineTransform(translationX: -cropRect.origin.x , y: 0)
       }*/
        
        // let t2 = t1.rotated(by: .pi / 2)
        // let finalTransform = /*(UIScreen.main.scale > 2 && isCropMode) ? t2.scaledBy(x: 1.15, y: 1.1) : */ t2
        
        let renderSize = frame.size
        let renderScale = renderSize.width / cropRect.width
        let offset = CGPoint(x: -cropRect.origin.x, y: -cropRect.origin.y)
        let rotation = atan2(clipVideoTrack.preferredTransform.b, clipVideoTrack.preferredTransform.a)
        var rotationOffset = CGPoint(x: 0, y: 0)
        
        if clipVideoTrack.preferredTransform.b == -1.0 {
            rotationOffset.y = clipVideoTrack.naturalSize.width
        } else if clipVideoTrack.preferredTransform.c == -1.0 {
            rotationOffset.x = clipVideoTrack.naturalSize.height
        } else if clipVideoTrack.preferredTransform.a == -1.0 {
            rotationOffset.x = clipVideoTrack.naturalSize.width
            rotationOffset.y = clipVideoTrack.naturalSize.height
        }
        
        var transform = CGAffineTransform.identity
        transform = transform.scaledBy(x: renderScale, y: renderScale)
        transform = transform.translatedBy(x: offset.x + rotationOffset.x, y: offset.y + rotationOffset.y)
        transform = transform.rotated(by: rotation)
        
        transformer.setTransform(transform, at: CMTime.zero)
        //transformer.setOpacity(1.0, at: kCMTimeZero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
        
        guard let exporter = AVAssetExportSession(asset: videeAsset, presetName: AVAssetExportPresetHighestQuality) else {
            fatalError("failed to create session.")
        }
        exporter.videoComposition = videoComposition
        exporter.outputURL = inputURL
        exporter.outputFileType = .mov
        
        exporter.exportAsynchronously {
            switch exporter.status {
            case .completed:
                completion(exporter.outputURL)
            default:
                completion(nil)
            }
        }
    }
}



//  MARK: - AKImageCropperViewDelegate

extension RVimageCropperViewController: AKImageCropperViewDelegate {
    
    func imageCropperViewDidChangeCropRect(view: AKImageCropperView, cropRect rect: CGRect) {
        cropRect = rect
             print("New crop rectangle: \(rect)")
    }
    
}
