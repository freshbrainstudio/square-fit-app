//
//  InitialVC.swift
//  SquareFit
//
//  Created by Qaiser Butt on 9/4/19.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import Photos
import MBProgressHUD


class InitialVC: UIViewController {
    
    var pickerController = UIImagePickerController()
    let gallery = GalleryController()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //FileManager.default.clearTmpDirectory()
    }
    
    // MARK: - Custom
    
    func imageSource(sourceType: String) {
        
        if sourceType == "video" {
            
            self.videoPick()
            
        }else if sourceType == "cameraRoll" {
            
           self.cameraRollPick()
            
        }else if sourceType == "camera" {
            
           self.cameraPick()
        }
    }
    
    func videoPick() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            pickerController.allowsEditing = true
            pickerController.delegate = self
            pickerController.mediaTypes = [kUTTypeMovie as String]
            pickerController.sourceType = .camera
            pickerController.videoQuality = .typeHigh
            pickerController.videoMaximumDuration = TimeInterval(300.0)
            
            self.present(self.pickerController, animated: true, completion: nil)
        }else {
            let alert = AlertView.prepare(title: "Alert", message:"Can't find camera", okAction: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func cameraRollPick() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            pickerController.delegate = self
            pickerController.sourceType = .photoLibrary
            pickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
            pickerController.allowsEditing = false
            pickerController.videoExportPreset = AVAssetExportPreset1920x1080
            self.present(pickerController, animated: true, completion: nil)

        }else {
            let alert = AlertView.prepare(title: "Alert", message:"Coudn't find Camera Roll", okAction: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func cameraPick() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            pickerController.delegate = self
            pickerController.mediaTypes = [kUTTypeImage as String]
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }else{
            let alert = AlertView.prepare(title: "Alert", message:"Coudn't find Camera Roll", okAction: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func TrimVideo(asset: AVAsset, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil)
        {
            let fileManager = FileManager.default
            let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

    //        let asset = AVAsset(url: sourceURL)
            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
            print("video length: \(length) seconds")

            var outputURL = documentDirectory.appendingPathComponent("output")
            do {
                try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                outputURL = outputURL.appendingPathComponent("trimmed.mp4")
            }catch let error {
                print(error)
            }

            //Remove existing file
            try? fileManager.removeItem(at: outputURL)

            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4

            let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                        end: CMTime(seconds: endTime, preferredTimescale: 1000))

            exportSession.timeRange = timeRange
            exportSession.exportAsynchronously {
                switch exportSession.status {
                case .completed:
                    print("exported at \(outputURL)")
                    completion?(outputURL)
                case .failed:
                    print("failed \(exportSession.error.debugDescription)")
                case .cancelled:
                    print("cancelled \(exportSession.error.debugDescription)")
                default: break
                }
            }
        }

    // MARK: - Navigation

    @IBAction func actionVideo(_ sender: UIButton) {
        
        self.imageSource(sourceType: "video")
        
    }
    @IBAction func actionCameraRoll(_ sender: UIButton) {
        //self.imageSource(sourceType: "cameraRoll")
        gallery.delegate = self
        Config.Camera.imageLimit = 1
        Config.tabsToShow = [Config.GalleryTab.imageTab,Config.GalleryTab.videoTab]
        Config.VideoEditor.quality = AVAssetExportPreset1920x1080
        Config.VideoEditor.maximumDuration = 100000
        gallery.modalPresentationStyle = .fullScreen
        present(gallery, animated: true, completion: nil)
    }
    @IBAction func actionCamera(_ sender: UIButton) {
        self.imageSource(sourceType: "camera")
    }
}
extension InitialVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func videoEditorController(_: UIVideoEditorController, didSaveEditedVideoToPath: String){
        print(didSaveEditedVideoToPath)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let customInfo = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let mediaType = customInfo[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaType)] as? String {
            if mediaType  == "public.image" {
                if let pickedImage = customInfo[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC
                    
                    vc?.myImg = pickedImage.fixOrientation()
                    vc?.mediaType = .photo
                    vc?.videoURL = nil
                    SharedData.SharedInfo.isEditable = false
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else if mediaType == "public.movie" {
                var finalURL:URL!
                if let url: URL = customInfo["UIImagePickerControllerMediaURL"] as? URL {
                    finalURL = url
                    let editingEnd = UIImagePickerController.InfoKey(rawValue: "_UIImagePickerControllerVideoEditingEnd")
                    let editingStart = UIImagePickerController.InfoKey(rawValue: "_UIImagePickerControllerVideoEditingStart")
                    if let start = info[editingStart] as? Double, let end = info[editingEnd] as? Double {
                        print("Trim Values",start,end)
                        TrimVideo(asset: AVAsset(url: finalURL), startTime: start, endTime: end) { (trimVideoUrl) in
                            finalURL = trimVideoUrl
                            DispatchQueue.main.async {
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC
                                vc?.myImg = nil
                                vc?.videoURL = trimVideoUrl
                                vc?.mediaType = .video
                                SharedData.SharedInfo.isEditable = false
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }
                        }
                    }
                }
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC
                vc?.myImg = nil
                vc?.videoURL = finalURL
                vc?.mediaType = .video
                SharedData.SharedInfo.isEditable = false
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }

        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

extension InitialVC:GalleryControllerDelegate{
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TrimVC") as? TrimVC
        DispatchQueue.main.async {
            video.asset.getURL { (url) in
                    vc?.videoUrl = url
                    SharedData.SharedInfo.isEditable = false
            }
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("DidSelect")
        controller.dismiss(animated: true, completion: nil)
        for image in images{
            let finalImage = getAssetThumbnail(asset: image.asset)
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? MainVC

            vc?.myImg = finalImage
            vc?.mediaType = .photo
            vc?.videoURL = nil
            SharedData.SharedInfo.isEditable = false

            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox")

    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension PHAsset {

    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
                let options: PHVideoRequestOptions = PHVideoRequestOptions()
                options.version = .original
                options.isNetworkAccessAllowed = true
                PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                    if let urlAsset = asset as? AVURLAsset {
                        let localVideoUrl: URL = urlAsset.url as URL
                        completionHandler(localVideoUrl)
                    } else {
                        completionHandler(nil)
                    }
                })
        }
    }
    
}
func getAssetThumbnail(asset: PHAsset) -> UIImage {
    let manager = PHImageManager.default()
    let option = PHImageRequestOptions()
    var thumbnail = UIImage()
    option.isSynchronous = true
    option.isNetworkAccessAllowed = true
    manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
    })
    return thumbnail
}
