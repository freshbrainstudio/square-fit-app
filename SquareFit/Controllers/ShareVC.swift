//
//  ShareVC.swift
//  SquareFit
//
//  Created by Fresh Brain on 17/09/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ShareVC: UIViewController,UIDocumentInteractionControllerDelegate{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var savedView: CardView!
    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var canvasWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasHeightConstraint: NSLayoutConstraint!

    var videoURL: URL? = nil
    var myImg = UIImage()
    var documentController: UIDocumentInteractionController!
    var mediaType = MediaType.photo
    var videoPlayer: AVPlayer? = nil
    var videoLayer: AVPlayerLayer? = nil
    var isVideoPlaying = false
    var firstRun = true
    var playButton: UIButton? = nil
    var PhotoOrVideo = ""
    var isPhotoOrVideoSaved = false
    var mcTransform: CGAffineTransform? = nil
    var mcCenter: CGPoint? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let videoPlayerGesture = UITapGestureRecognizer(target: self, action:  #selector (self.hidePlayPauseButton (_:)))
        self.videoContainer.addGestureRecognizer(videoPlayerGesture)
        self.imageView.image = myImg
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        editingView.isHidden = true
        savedView.setDownView(view: savedView, hidden: true)
        
        if SharedData.SharedInfo.isBackgroundColorChanged{
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
            canvasView.layer.contents = nil
        }
        if mediaType == .photo{
            perform(#selector(resizeCanvasView), with: nil, afterDelay: 0.1)
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
//            videoContainer.backgroundColor = SharedData.SharedInfo.backgroundColor
            videoContainer.isHidden = true
            PhotoOrVideo = "photo"
        }else{
            PhotoOrVideo = "video"
            imageView.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstRun {
            firstRun = false
            
            if mediaType == .video {
                videoContainer.backgroundColor = UIColor.clear
                canvasView.backgroundColor = UIColor.clear
                perform(#selector(setUpVideoContainer), with: nil, afterDelay: 0.1)
            } else {
                if SharedData.SharedInfo.isBlurred {
                    let intensity = SharedData.SharedInfo.blurIntensity
                    let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: myImg, isVideoProcessing: false) as? UIImage
                    
                    canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
                    canvasView.layer.contents = uiimage?.cgImage
                }
                
                imageView.transform = mcTransform ?? imageView.transform
                perform(#selector(reCenter), with: nil, afterDelay: 0.0)
            }
            
            self.editingView.isHidden = false
        }
    }
    
    @objc func reCenter() {
        imageView.center = mcCenter ?? imageView.center
    }
    
    @objc func hidePlayPauseButton(_ sender:UITapGestureRecognizer){
        print("Play")
        if videoPlayer?.timeControlStatus == .playing{
            videoPlayer?.pause()
            self.playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
            self.playButton?.isHidden = false

        }else if videoPlayer?.timeControlStatus == .paused{
            videoPlayer?.play()
            self.playButton?.setImage(UIImage(named: "pause_btn"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.playButton?.isHidden = true
            }
        }
    }
    func saveVideoToGallery(){
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.videoURL!)
        }) { saved, error in
            if saved {
                self.isPhotoOrVideoSaved = true
                DispatchQueue.main.async {
                    self.saveLabel.text = "Saved"
                    self.savedView.setUpView(view: self.savedView, hidden: false)
                }
                let now = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: now){
                    self.savedView.setDownView(view: self.savedView, hidden: true)
                }
            }else{
                self.isPhotoOrVideoSaved = false
            }
        }
    }

    @objc func resizeCanvasView() {
        let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: imageView.image?.size ?? CGSize.zero)
        canvasWidthConstraint.constant = size.width
        canvasHeightConstraint.constant = size.height
    }
    
    @objc func setUpVideoContainer() {
        if let vidUrl = videoURL {
            let asset = AVURLAsset(url: vidUrl, options: nil)
            
            if let track = asset.tracks(withMediaType: AVMediaType.video).first {
                let mSize = __CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform)
                let mediaSize = CGSize(width: fabs(Double(mSize.width)), height: fabs(Double(mSize.height)))
                let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: mediaSize)
                
                canvasWidthConstraint.constant = size.width
                canvasHeightConstraint.constant = size.height
                perform(#selector(setUpVideoPlayer), with: nil, afterDelay: 0.0)
            }
        }
    }
    
    @objc func setUpVideoPlayer() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            let layerSize = videoContainer.frame.size
            guard let videoUrl = videoURL else {
                return
            }
            print(layerSize)
            videoPlayer = AVPlayer(url: videoUrl)
            videoLayer = AVPlayerLayer(player: videoPlayer)
            videoLayer?.videoGravity = .resizeAspect
            videoLayer?.frame = CGRect(x: 0, y: 0, width: layerSize.width, height: layerSize.height)
            videoContainer.layer.addSublayer(videoLayer!)
            
            videoPlayer?.seek(to: CMTime.zero)
            videoPlayer?.volume = 1.0
            videoPlayer?.actionAtItemEnd = .none
            
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd),
                                                   name: .AVPlayerItemDidPlayToEndTime, object: nil)
            
            let tempImg = UIImage(named: "play_btn")
            
            playButton = UIButton(type: .custom)
            playButton?.setImage(tempImg, for: .normal)
            playButton?.addTarget(self, action: #selector(playVideo), for: .touchUpInside)
            playButton?.frame = CGRect(origin: CGPoint.zero, size: tempImg?.size ?? CGSize(width: 50, height: 50))
            playButton?.center = CGPoint(x: layerSize.width / 2.0, y: layerSize.height / 2.0)
            
            if let pButton = playButton {
                videoContainer.addSubview(pButton)
            }
    } catch {
            print(error)
        }
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if let player = videoPlayer {
            player.seek(to: .zero)
            player.pause()
        }
        playButton?.isHidden = false
        isVideoPlaying = false
        playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
    }
    @objc fileprivate func playVideo(_ sender: Any) {
           if videoPlayer?.timeControlStatus == .playing{
               videoPlayer?.pause()
               self.playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
               self.playButton?.isHidden = false

           }else if videoPlayer?.timeControlStatus == .paused{
               videoPlayer?.play()
               self.playButton?.setImage(UIImage(named: "pause_btn"), for: .normal)
               DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                   self.playButton?.isHidden = true
               }
           }
       }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            let alert = AlertView.prepare(title: "Save error", message:"There is some error while saving edited picture to gallery.", okAction: nil)
            self.present(alert, animated: true, completion: nil)
            isPhotoOrVideoSaved = false
        } else {
            isPhotoOrVideoSaved = true
            saveLabel.text = "Saved"
            self.savedView.setUpView(view: savedView, hidden: false)
            let now = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: now){
                self.savedView.setDownView(view: self.savedView, hidden: true)
            }
        }
    }
    
    // MARK: -  Share To Instagram
    
    @IBAction func shareToInstagram(_ sender: Any) {
        if mediaType == .photo{
            if (self.imageView.image != nil) {
               let instagramURL = URL(string: "instagram://app")
               if UIApplication.shared.canOpenURL(instagramURL!) {
                   let imageData = self.canvasView.asImage().jpegData(compressionQuality: 100)
                   let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
                   do {
                       try imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic)
                   } catch {
                       print(error)
                   }
                   let fileURL = URL(fileURLWithPath: writePath)
                   self.documentController = UIDocumentInteractionController(url: fileURL)
                   self.documentController.delegate = self
                   self.documentController.uti = "com.instagram.exlusivegram"
                   self.documentController.presentOpenInMenu(from: self.view.bounds, in: self.view, animated: true)

               } else {
                   let alert = AlertView.prepare(title: "Instagram Not Found!!", message: "Instagram not installed in this device!\nTo share image please install instagram.", okAction: nil)
                   self.present(alert, animated: true, completion: nil)               }
               return
            }
        }else{
            if isPhotoOrVideoSaved{
                guard let finalVideoUrl = videoURL else { return }
                let escapedString:String = finalVideoUrl.absoluteString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
                let escapedCaption:String = "Square Fit".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
                let instagramURL:URL = URL.init(string: "instagram://library?AssetPath=" + escapedString + "&InstagramCaption=" + escapedCaption)!
                
                if (UIApplication.shared.canOpenURL(instagramURL)) {
                    UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
                }else {
                    let alert = AlertView.prepare(title: "Instagram Not Found!!", message: "Instagram not installed in this device!\nTo share image please install instagram.", okAction: nil)
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = AlertView.prepare(title: "Video Not Saved", message: "Save video First", okAction: nil)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func shareToFacebook(_ sender: Any) {
        let facebookUrl = URL(string: "fb://")
        if UIApplication.shared.canOpenURL(facebookUrl!) {
            self.savedView.setDownView(view: savedView, hidden: true)
            let activityVC :UIActivityViewController

            if mediaType == .photo{
            
                   var finalImage = myImg
                   let rectsize = CGRect(origin: CGPoint.zero, size: finalImage.size)
                   var backgroundImage: CIImage? = nil
                   if SharedData.SharedInfo.isBlurred {
                       let intensity = SharedData.SharedInfo.blurIntensity
                       let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: finalImage, isVideoProcessing: false) as? UIImage
                       if let cgImage = uiimage?.cgImage {
                           backgroundImage = CIImage(cgImage: cgImage)
                           
                       }
                   } else {
                       let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                       let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                       
                       backgroundImage = colorBackgroundImg
                   }
                   
                   let imagesize = canvasView.frame.size
                   let reqImageSize = imageView.frame.size
                   let wScaleRatio = reqImageSize.width / imagesize.width
                   let hScaleRatio = reqImageSize.height / imagesize.height
                   let currentImageHeight = finalImage.size.height * hScaleRatio
                   let bgsize = backgroundImage?.extent.size ?? CGSize.zero
                   let xCanvasMediaRatio = imageView.frame.origin.x / canvasView.frame.size.width
                   let yCanvasMediaRatio = imageView.frame.origin.y / canvasView.frame.size.height
                   let translationX = bgsize.width * xCanvasMediaRatio
                   /* Reverse calculating height while exporting due to inverse coordinate system */
                   let translationY = bgsize.height - currentImageHeight - (bgsize.height * yCanvasMediaRatio)
                   //let xx = rectsize.width - (rectsize.width * wScaleRatio)
                   //let yy = rectsize.height - (rectsize.height * hScaleRatio)
                   
                   if let cgimg = finalImage.cgImage {
                       var image = CIImage(cgImage: cgimg)
                       
                       image = image.transformed(by: CGAffineTransform(scaleX: wScaleRatio, y: hScaleRatio))
                       image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                       
                       if let bgimage = backgroundImage {
                           image = image.composited(over: bgimage)
                       }
                       
                       finalImage = Utils.convertToUIImage(origImg: image, scale: finalImage.scale, imageOrientation: finalImage.imageOrientation)
                   }
                   
               
                 activityVC =
                             UIActivityViewController(activityItems: [finalImage],
                                                      applicationActivities: nil)
             }else{
                guard let finalVideoUrl = videoURL else { return }
                let activityItems: [Any] = [finalVideoUrl, "Check this out!"]
                activityVC =
                            UIActivityViewController(activityItems: activityItems,
                                                     applicationActivities: nil)
            }
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.excludedActivityTypes = [.postToTwitter]
            present(activityVC, animated: true, completion: nil)
        }else{
             let alert = AlertView.prepare(title: "Facebook Not Found!!", message: "Facebook not installed in this device!\nTo share image please install Facebook.", okAction: nil)
                           self.present(alert, animated: true, completion: nil)
        }
//        UIImageWriteToSavedPhotosAlbum(self.imgBG.asImage(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
//        if self.imageView.image != nil {
//            UIImageWriteToSavedPhotosAlbum(self.imageView.asImage(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
//        }
    }
    
    @IBAction func shareToTwitter(_ sender: Any) {
        self.savedView.setDownView(view: savedView, hidden: true)
        let activityVC :UIActivityViewController
        if mediaType == .photo{
        
               var finalImage = myImg
               let rectsize = CGRect(origin: CGPoint.zero, size: finalImage.size)
               var backgroundImage: CIImage? = nil
               if SharedData.SharedInfo.isBlurred {
                   let intensity = SharedData.SharedInfo.blurIntensity
                   let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: finalImage, isVideoProcessing: false) as? UIImage
                   if let cgImage = uiimage?.cgImage {
                       backgroundImage = CIImage(cgImage: cgImage)
                       
                   }
               } else {
                   let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                   let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                   
                   backgroundImage = colorBackgroundImg
               }
               
               let imagesize = canvasView.frame.size
               let reqImageSize = imageView.frame.size
               let wScaleRatio = reqImageSize.width / imagesize.width
               let hScaleRatio = reqImageSize.height / imagesize.height
               let currentImageHeight = finalImage.size.height * hScaleRatio
               let bgsize = backgroundImage?.extent.size ?? CGSize.zero
               let xCanvasMediaRatio = imageView.frame.origin.x / canvasView.frame.size.width
               let yCanvasMediaRatio = imageView.frame.origin.y / canvasView.frame.size.height
               let translationX = bgsize.width * xCanvasMediaRatio
               /* Reverse calculating height while exporting due to inverse coordinate system */
               let translationY = bgsize.height - currentImageHeight - (bgsize.height * yCanvasMediaRatio)
               //let xx = rectsize.width - (rectsize.width * wScaleRatio)
               //let yy = rectsize.height - (rectsize.height * hScaleRatio)
               
               if let cgimg = finalImage.cgImage {
                   var image = CIImage(cgImage: cgimg)
                   
                   image = image.transformed(by: CGAffineTransform(scaleX: wScaleRatio, y: hScaleRatio))
                   image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                   
                   if let bgimage = backgroundImage {
                       image = image.composited(over: bgimage)
                   }
                   
                   finalImage = Utils.convertToUIImage(origImg: image, scale: finalImage.scale, imageOrientation: finalImage.imageOrientation)
               }
               
           
             activityVC =
                         UIActivityViewController(activityItems: [finalImage],
                                                  applicationActivities: nil)
         }else{
            guard let finalVideoUrl = videoURL else { return }
            let activityItems: [Any] = [finalVideoUrl, "Check this out!"]
            activityVC =
                        UIActivityViewController(activityItems: activityItems,
                                                 applicationActivities: nil)
        }
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.excludedActivityTypes = [.postToFacebook]
        present(activityVC, animated: true) {
         }
    }
    
    @IBAction func actionOtherShare(_ sender: Any) {
        
        self.savedView.setDownView(view: savedView, hidden: true)

       let activityVC :UIActivityViewController
        if mediaType == .photo{
       
              var finalImage = myImg
              let rectsize = CGRect(origin: CGPoint.zero, size: finalImage.size)
              var backgroundImage: CIImage? = nil
              if SharedData.SharedInfo.isBlurred {
                  let intensity = SharedData.SharedInfo.blurIntensity
                  let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: finalImage, isVideoProcessing: false) as? UIImage
                  if let cgImage = uiimage?.cgImage {
                      backgroundImage = CIImage(cgImage: cgImage)
                      
                  }
              } else {
                  let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                  let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                  
                  backgroundImage = colorBackgroundImg
              }
              
              let imagesize = canvasView.frame.size
              let reqImageSize = imageView.frame.size
              let wScaleRatio = reqImageSize.width / imagesize.width
              let hScaleRatio = reqImageSize.height / imagesize.height
              let currentImageHeight = finalImage.size.height * hScaleRatio
              let bgsize = backgroundImage?.extent.size ?? CGSize.zero
              let xCanvasMediaRatio = imageView.frame.origin.x / canvasView.frame.size.width
              let yCanvasMediaRatio = imageView.frame.origin.y / canvasView.frame.size.height
              let translationX = bgsize.width * xCanvasMediaRatio
              /* Reverse calculating height while exporting due to inverse coordinate system */
              let translationY = bgsize.height - currentImageHeight - (bgsize.height * yCanvasMediaRatio)
              //let xx = rectsize.width - (rectsize.width * wScaleRatio)
              //let yy = rectsize.height - (rectsize.height * hScaleRatio)
              
              if let cgimg = finalImage.cgImage {
                  var image = CIImage(cgImage: cgimg)
                  
                  image = image.transformed(by: CGAffineTransform(scaleX: wScaleRatio, y: hScaleRatio))
                  image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                  
                  if let bgimage = backgroundImage {
                      image = image.composited(over: bgimage)
                  }
                  
                  finalImage = Utils.convertToUIImage(origImg: image, scale: finalImage.scale, imageOrientation: finalImage.imageOrientation)
              }
              
          
            activityVC =
                        UIActivityViewController(activityItems: [finalImage],
                                                 applicationActivities: nil)
        }else{
            guard let finalVideoUrl = videoURL else { return }
            let activityItems: [Any] = [finalVideoUrl]
            activityVC =
                        UIActivityViewController(activityItems: activityItems,
                                                 applicationActivities: nil)
        }
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.excludedActivityTypes = [.postToTwitter]
        present(activityVC, animated: true) {
         }
        
    }
    @IBAction func actionSaveToGallery(_ sender: Any) {
        if isPhotoOrVideoSaved {
            let alert = AlertView.prepare(title: "Saved", message: "\(PhotoOrVideo) already saved.", okAction: nil)
            self.present(alert, animated: true, completion: nil)
            //saveButton.isEnabled = false
        } else {
            if mediaType == .photo {
                var finalImage = myImg
                let rectsize = CGRect(origin: CGPoint.zero, size: finalImage.size)
                var backgroundImage: CIImage? = nil
                if SharedData.SharedInfo.isBlurred {
                    let intensity = SharedData.SharedInfo.blurIntensity
                    let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: finalImage, isVideoProcessing: false) as? UIImage
                    if let cgImage = uiimage?.cgImage {
                        backgroundImage = CIImage(cgImage: cgImage)
                        
                    }
                } else {
                    let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                    let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                    
                    backgroundImage = colorBackgroundImg
                    
                }
                
                let imagesize = canvasView.frame.size
                let reqImageSize = imageView.frame.size
                let wScaleRatio = reqImageSize.width / imagesize.width
                let hScaleRatio = reqImageSize.height / imagesize.height
                let currentImageHeight = finalImage.size.height * hScaleRatio
                let bgsize = backgroundImage?.extent.size ?? CGSize.zero
                let xCanvasMediaRatio = imageView.frame.origin.x / canvasView.frame.size.width
                let yCanvasMediaRatio = imageView.frame.origin.y / canvasView.frame.size.height
                let translationX = bgsize.width * xCanvasMediaRatio
                /* Reverse calculating height while exporting due to inverse coordinate system */
                let translationY = bgsize.height - currentImageHeight - (bgsize.height * yCanvasMediaRatio)
                //let xx = rectsize.width - (rectsize.width * wScaleRatio)
                //let yy = rectsize.height - (rectsize.height * hScaleRatio)
                
                if let cgimg = finalImage.cgImage {
                    var image = CIImage(cgImage: cgimg)
                    
                    image = image.transformed(by: CGAffineTransform(scaleX: wScaleRatio, y: hScaleRatio))
                    image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                    
                    if let bgimage = backgroundImage {
                        image = image.composited(over: bgimage)
                    }
                    
                    finalImage = Utils.convertToUIImage(origImg: image, scale: finalImage.scale, imageOrientation: finalImage.imageOrientation)
                }
               
                UIImageWriteToSavedPhotosAlbum(finalImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            } else {
                saveVideoToGallery()
            }
        }
    }
    
    @IBAction func actionDone(_ sender: Any) {
            if isPhotoOrVideoSaved == false{
                let alert = UIAlertController(title: "Exit", message: "Are you sure you want to exit without saving this \(PhotoOrVideo) ?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Exit", style: .destructive, handler: { (action) in
                    self.resetSharedData()
                    self.popToRootVC()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                resetSharedData()
                self.popToRootVC()
            }
        
    }
    @IBAction func actionBack(_ sender: Any) {
        self.savedView.setDownView(view: savedView, hidden: true)
        self.popVC()
    }
    
    func resetSharedData() {
        SharedData.SharedInfo.isBackgroundColorChanged = false
        SharedData.SharedInfo.isBlurred = false
        SharedData.SharedInfo.isEditable = false
        SharedData.SharedInfo.blurIntensity = 0.0
        SharedData.SharedInfo.isFiltered = false
        SharedData.SharedInfo.filterUsed = BPFilter.none
        SharedData.SharedInfo.isTxtEnterd = false
        SharedData.SharedInfo.enteredTxt = ""
        SharedData.SharedInfo.isCroped = false
        SharedData.SharedInfo.updateTextImageLayer = false
        SharedData.SharedInfo.isEditingStarted = false
        SharedData.SharedInfo.globalEditedImage = UIImage()
        SharedData.SharedInfo.backgroundColor = UIColor.white
        SharedData.SharedInfo.selectedIndexPathForFilters = IndexPath(item: 0, section: 0)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
}
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
