//
//  HomeVC.swift
//  SquareFit
//
//  Created by Qaiser Butt on 9/4/19.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//


import UIKit
import AVFoundation
import UICircularProgressRing

class MainVC: UIViewController, CanvasCropedProtocole {
    @IBOutlet weak var canvasToolBarView: UIView!
    @IBOutlet weak var canvasSlider: UISlider!
    @IBOutlet weak var canvasSizeCollectionView: UICollectionView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var progressView: UICircularProgressRing!
    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var imageContainerView: UIImageView!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var canvasWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasHeightConstraint: NSLayoutConstraint!
    
    var nameArr = ["9:16", "1:1", "4:5", "16:9", "2:3", "3:4", "4:3", "6:4", "4:6", "7:6", "6:7", "10:8", "8:10"]
    var photoImages = [UIImage(named: "9-16"),UIImage(named: "1-1"),UIImage(named: "4-5"), UIImage(named: "16-9"),
                       UIImage(named: "2-3"), UIImage(named: "9-16"),UIImage(named: "1-1"),UIImage(named: "4-5"),
                       UIImage(named: "2-3"), UIImage(named: "9-16"),UIImage(named: "1-1"),UIImage(named: "4-5"), UIImage(named: "4-5")]
    
    private var bottomBarIcons = [UIImage(named: "iconcanvas.png"),UIImage(named: "iconColor"),
                                  UIImage(named: "iconBlur"), UIImage(named: "iconFilter"), UIImage(named: "iconAa")]
    private var bottomBarLabels = ["Canvas", "Color", "Blur", "Filter", "Text"]
    var myImg: UIImage? = nil
    var textWatermarkImg: UIImage? = nil
    var mediaType = MediaType.photo
    var videoPlayer: AVPlayer? = nil
    var videoLayer: AVPlayerLayer? = nil
    var videoURL: URL? = nil
    var videoThumbnail: UIImage? = nil
    var isVideoPlaying = false
    var playButton: UIButton? = nil
    var videoComposition: AVMutableVideoComposition? = nil
    var generatingFinalVideo = false
    var firstRun = true
    var canvasResized = true
    var exportProgressBarTimer: Timer? = nil
    var exporterSession: AVAssetExportSession? = nil
    var wCanvasMediaRatio: CGFloat = 0.0
    var hCanvasMediaRatio: CGFloat = 0.0
    var xCanvasMediaRatio: CGFloat = 0.0
    var yCanvasMediaRatio: CGFloat = 0.0
    var selectedCanvasRatio = "1:1"
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let videoPlayerGesture = UITapGestureRecognizer(target: self, action:  #selector (self.hidePlayPauseButton (_:)))
        let pinchGesture1 = UIPinchGestureRecognizer(target: self, action: #selector(pinchView(sender:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchView(sender:)))
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panView(sender:)))
        let panGesture1 = UIPanGestureRecognizer(target: self, action: #selector(panView(sender:)))
        
        imageContainerView.addGestureRecognizer(panGesture)
        imageContainerView.addGestureRecognizer(pinchGesture)
        videoContainerView.addGestureRecognizer(panGesture1)
        videoContainerView.addGestureRecognizer(pinchGesture1)
        videoContainerView.addGestureRecognizer(videoPlayerGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if SharedData.SharedInfo.isBackgroundColorChanged {
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
            canvasView.layer.contents = nil
        }
        if mediaType == .video {
            loaderView.isHidden = true
            loaderView.alpha = 0.0
            
            if SharedData.SharedInfo.isCroped {
                videoURL = SharedData.SharedInfo.trimmedAndCroppedVideoUrl
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstRun {
            firstRun = false
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            blurEffectView.frame = loaderView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            loaderView.addSubview(blurEffectView)
            loaderView.sendSubviewToBack(blurEffectView)
        }
        
        if mediaType == .photo {
            let isEditable = SharedData.SharedInfo.isEditable
            let img = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
            
            if SharedData.SharedInfo.isBlurred {
                let intensity = SharedData.SharedInfo.blurIntensity
                let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: img!, isVideoProcessing: false) as? UIImage
                
                canvasView.backgroundColor = UIColor.clear
                canvasView.layer.contents = uiimage?.cgImage
                canvasView.layer.contentsGravity = .resizeAspectFill
                videoContainerView.isHidden = true
            }
            
            imageContainerView.image = img
            videoContainerView.isHidden = true
            
            let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: imageContainerView.image?.size ?? CGSize.zero)
            canvasWidthConstraint.constant = size.width
            canvasHeightConstraint.constant = size.height
            //SharedData.SharedInfo.isEditable = false
            
            resizeCanvas(canvasRatio: selectedCanvasRatio)
        } else if mediaType == .video {
            if SharedData.SharedInfo.updateTextImageLayer {
                SharedData.SharedInfo.updateTextImageLayer = false
                textWatermarkImg = SharedData.SharedInfo.globalEditedImage
            }
            
            playButton?.isHidden = false
            generatingFinalVideo = false
            imageContainerView.isHidden = true
            videoContainerView.isHidden = false
            
            perform(#selector(setUpVideoContainer), with: nil, afterDelay: 0.1)
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func applyCanvas(_ sender: Any) {
        canvasToolBarView.isHidden = true
    }
    
    @IBAction func closeCanvas(_ sender: Any) {
        canvasToolBarView.isHidden = true
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        if SharedData.SharedInfo.isEditingStarted{
            let alert = UIAlertController(title: "Discard Changes", message: "All changes will be discarded", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Discard", style: .destructive, handler: { (action) in
                SharedData.SharedInfo.isBlurred = false
                SharedData.SharedInfo.isEditable = false
                SharedData.SharedInfo.isEditingStarted = false
                SharedData.SharedInfo.blurIntensity = 0.0
                SharedData.SharedInfo.isFiltered = false
                SharedData.SharedInfo.filterUsed = BPFilter.none
                SharedData.SharedInfo.isTxtEnterd = false
                SharedData.SharedInfo.enteredTxt = ""
                SharedData.SharedInfo.isCroped = false
                SharedData.SharedInfo.updateTextImageLayer = false
                SharedData.SharedInfo.globalEditedImage = UIImage()
                SharedData.SharedInfo.backgroundColor = UIColor.white
                
                self.popToRootVC()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            popToRootVC()
        }
    }
    @IBAction func actionSave(_ sender: UIButton) {
        if mediaType == .video {
            canvasToolBarView.isHidden = true
            let imagesize = canvasView.frame.size
            let reqImageSize = videoContainerView.frame.size
            
            xCanvasMediaRatio = videoContainerView.frame.origin.x / canvasView.frame.size.width
            yCanvasMediaRatio = videoContainerView.frame.origin.y / canvasView.frame.size.height
            wCanvasMediaRatio = reqImageSize.width / imagesize.width
            hCanvasMediaRatio = reqImageSize.height / imagesize.height
            generatingFinalVideo = true
            
            videoPlayer?.pause()
            videoPlayer?.seek(to: CMTime.zero)
            playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
            savingVideoWithEffects { (url, error) in
                if url != nil {
                    DispatchQueue.main.async {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShareVC") as? ShareVC
                        vc?.videoURL = url
                        vc?.mediaType = .video
                        vc?.isPhotoOrVideoSaved = false
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
            }
        } else {
            let finalImage = SharedData.SharedInfo.isEditable ? SharedData.SharedInfo.globalEditedImage : self.myImg ?? UIImage.init()
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShareVC") as? ShareVC
            
            vc?.mcTransform = imageContainerView.transform
            vc?.mcCenter = imageContainerView.center
            vc?.myImg = finalImage
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @IBAction func cancelExportButton(_ sender: Any) {
        exporterSession?.cancelExport()
        generatingFinalVideo = false
        exportProgressBarTimer?.invalidate()
        exportProgressBarTimer = nil
        loaderView.isHidden = true
        loaderView.alpha = 0.0
        perform(#selector(setUpVideoContainer), with: nil, afterDelay: 0.1)
        
    }
    
    func addOverlayImage(overlayImage: UIImage) {
        let overlayLayer = CALayer()
        overlayLayer.contents = overlayImage.cgImage
    }
    
    private func savingVideoWithEffects(completion: @escaping (URL?, Error?) -> ()) {
        let fileName = UUID().uuidString
        let tempURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName).appendingPathExtension("mp4")
        let asset = AVAsset(url: videoURL!)
        
        exportProgressBarTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateExportProgress), userInfo: nil, repeats: true)
        var metadata: [AnyHashable] = []
        let metaItem = AVMutableMetadataItem()
        metadata.append(metaItem)
        
        guard let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPreset1920x1080) else {
            fatalError("failed to create session.")
        }
        exporter.videoComposition = videoComposition
        exporter.outputURL = tempURL
        exporter.outputFileType = .mp4
        exporter.metadata = metadata as? [AVMetadataItem]
        
        exporter.exportAsynchronously {
            switch exporter.status {
            case .completed:
                self.exportProgressBarTimer?.invalidate()
                self.exportProgressBarTimer = nil
                
                //                DispatchQueue.main.async {
                //                    self.loaderView.alpha = 0.0
                //                    self.loaderView.isHidden = true
                //                }
                
                print("Export Completed")
                
                completion(exporter.outputURL, nil)
            case .cancelled:
                self.exportProgressBarTimer?.invalidate()
                self.exportProgressBarTimer = nil
                
                self.loaderView.isHidden = true
                self.loaderView.alpha = 0.0
                completion(nil,exporter.error)
            default:
                completion(nil, exporter.error)
            }
        }
        
        exporterSession = exporter
        
        progressView.delegate = self
        progressView.value = 0.0
        loaderView.isHidden = false
        loaderView.alpha = 1.0
    }
    
    
    @objc func panView(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: canvasView)
        sender.view?.center = CGPoint(x: (sender.view?.center.x)! + translation.x, y: (sender.view?.center.y)! + translation.y)
        sender.setTranslation(CGPoint.zero, in: canvasView)
    }
    
    @objc func pinchView(sender: UIPinchGestureRecognizer) {
        if let transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)) {
            sender.view?.transform = transform
            sender.scale = 1.0
        }
    }
    
    @objc func updateExportProgress() {
        if let session = exporterSession {
            let progressValue = CGFloat(session.progress)
            progressView.value = progressValue * 100
            
            print(session.progress)
            
            //            if (progressValue > 0.99) {
            //                exportProgressBarTimer?.invalidate()
            //                exportProgressBarTimer = nil
            //
            //                //loaderView.isHidden = true
            //                //loaderView.alpha = 0.0
            //                print("Progress Hidden")
            //            }
        }
    }
    
    @objc func hidePlayPauseButton(_ sender:UITapGestureRecognizer){
        if videoPlayer?.timeControlStatus == .playing{
            videoPlayer?.pause()
            playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
            playButton?.isHidden = false
        } else if videoPlayer?.timeControlStatus == .paused{
            videoPlayer?.play()
            playButton?.setImage(UIImage(named: "pause_btn"), for: .normal)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.playButton?.isHidden = true
            }
        }
    }
    
    @objc func generateVideoThumbnail() {
        videoThumbnail = Utils.getVideoThumbnail(from: videoURL!)
        
        if SharedData.SharedInfo.isBlurred {
            let intensity = SharedData.SharedInfo.blurIntensity
            let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: videoThumbnail!, isVideoProcessing: false) as? UIImage
            
            canvasView.backgroundColor = UIColor.clear
            canvasView.layer.contents = uiimage?.cgImage
            canvasView.layer.contentsGravity = .resizeAspectFill
        }
        
        /*if SharedData.SharedInfo.isBlurred {
         if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
         var image = CIImage(cgImage: vcgImage)
         let blurIntensity = SharedData.SharedInfo.blurIntensity
         
         image = Utils.filterWithGaussianBlur(radius: blurIntensity, imgObject: image, isVideoProcessing: true) as! CIImage
         
         if SharedData.SharedInfo.isFiltered {
         let bpfilter = SharedData.SharedInfo.filterUsed
         
         if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
         image = img
         }
         }
         if SharedData.SharedInfo.isTxtEnterd {
         if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
         let wCIImage = CIImage(cgImage: wcg)
         image = wCIImage.composited(over: image)
         }
         }
         
         videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
         imageOrientation: videoThumbnail?.imageOrientation ?? .up)
         }
         } else*/ if SharedData.SharedInfo.isFiltered {
            if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
                var image = CIImage(cgImage: vcgImage)
                let bpfilter = SharedData.SharedInfo.filterUsed
                
                if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                    image = img
                }
                if SharedData.SharedInfo.isTxtEnterd {
                    if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                        let wCIImage = CIImage(cgImage: wcg)
                        image = wCIImage.composited(over: image)
                    }
                }
                
                videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
                                                        imageOrientation: videoThumbnail?.imageOrientation ?? .up)
            }
         } else if SharedData.SharedInfo.isTxtEnterd {
            if let vThumbnail = videoThumbnail, let vcgImage = vThumbnail.cgImage {
                var image = CIImage(cgImage: vcgImage)
                if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                    let wCIImage = CIImage(cgImage: wcg)
                    image = wCIImage.composited(over: image)
                }
                
                videoThumbnail = Utils.convertToUIImage(origImg: image, scale: videoThumbnail?.scale ?? 1.0,
                                                        imageOrientation: videoThumbnail?.imageOrientation ?? .up)
            }
        }
    }
    
    @objc func setUpVideoContainer() {
        if let vidUrl = videoURL {
            let asset = AVURLAsset(url: vidUrl, options: nil)
            
            if let track = asset.tracks(withMediaType: AVMediaType.video).first {
                let mSize = __CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform)
                let mediaSize = CGSize(width: fabs(Double(mSize.width)), height: fabs(Double(mSize.height)))
                let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: mediaSize)
                
                canvasWidthConstraint.constant = size.width
                canvasHeightConstraint.constant = size.height
                resizeCanvas(canvasRatio: selectedCanvasRatio)
                
                if SharedData.SharedInfo.isCroped {
                    videoContainerView.transform = CGAffineTransform.identity
                    textWatermarkImg = SharedData.SharedInfo.croppedTextWatermarkImage
                }
                
                perform(#selector(generateVideoThumbnail))
                perform(#selector(setupVideoPlayer), with: nil, afterDelay: 0.1)
            }
        }
    }
    
    @objc func setupVideoPlayer() {
        do {
            if let url = videoURL {
                if videoPlayer == nil {
                    try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    let layerSize = videoContainerView.frame.size
                    
                    videoPlayer = AVPlayer(url: url)
                    videoLayer = AVPlayerLayer(player: videoPlayer)
                    videoLayer?.videoGravity = .resizeAspectFill
                    videoLayer?.frame = CGRect(x: 0, y: 0, width: layerSize.width, height: layerSize.height)
                    videoContainerView.layer.addSublayer(videoLayer!)
                    
                    videoPlayer?.seek(to: CMTime.zero)
                    videoPlayer?.volume = 1.0
                    videoPlayer?.actionAtItemEnd = .none
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd),
                                                           name: .AVPlayerItemDidPlayToEndTime, object: nil)
                    
                    let tempImg = UIImage(named: "play_btn")
                    
                    playButton = UIButton(type: .custom)
                    playButton?.setImage(tempImg, for: .normal)
                    playButton?.addTarget(self, action: #selector(playVideo), for: .touchUpInside)
                    playButton?.frame = CGRect(origin: CGPoint.zero, size: tempImg?.size ?? CGSize(width: 50, height: 50))
                    playButton?.center = CGPoint(x: layerSize.width / 2.0, y: layerSize.height / 2.0)
                    
                    if let pButton = playButton {
                        videoContainerView.addSubview(pButton)
                    }
                    let avAsset = AVAsset(url: url)
                    let rectsize = CGRect(origin: CGPoint.zero, size: videoThumbnail?.size ?? CGSize.zero)
                    let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                    let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                    let imagesize = canvasView.frame.size
                    let reqImageSize = videoContainerView.frame.size
                    
                    wCanvasMediaRatio = reqImageSize.width / imagesize.width
                    hCanvasMediaRatio = reqImageSize.height / imagesize.height
                    
                    let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
                        var image = request.sourceImage
                        
                        if self.generatingFinalVideo {
                            let currentImageHeight = image.extent.size.height * self.hCanvasMediaRatio
                            let bgsize = colorBackgroundImg.extent.size
                            let translationX = bgsize.width * self.xCanvasMediaRatio
                            /* Reverse calculating height while exporting due to inverse coordinate system */
                            let translationY = bgsize.height - currentImageHeight - (bgsize.height * self.yCanvasMediaRatio)
                            //let xx = colorBackgroundImg.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                            //let yy = colorBackgroundImg.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                            
                            image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                            image = image.transformed(by: CGAffineTransform(translationX: translationX /* + (xx/2.0)*/, y: translationY /* + (yy/2.0)*/))
                            image = image.composited(over: colorBackgroundImg)
                        }
                        
                        request.finish(with: image, context: nil)
                    })
                    
                    videoComposition = vidComp
                    
                    let playerItem = AVPlayerItem(asset: avAsset)
                    playerItem.videoComposition = videoComposition
                    videoPlayer?.replaceCurrentItem(with: playerItem)
                    
                    
                } else if SharedData.SharedInfo.isBlurred {
                    let avAsset = AVAsset(url: url)
                    let imagesize = canvasView.frame.size
                    let reqImageSize = videoContainerView.frame.size
                    var backgroundImage: CIImage? = nil
                    if let thumbnail = videoThumbnail {
                        let intensity = SharedData.SharedInfo.blurIntensity
                        let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: thumbnail, isVideoProcessing: false) as? UIImage
                        
                        if let cgImage = uiimage?.cgImage {
                            backgroundImage = CIImage(cgImage: cgImage)
                        }
                    }
                    
                    wCanvasMediaRatio = reqImageSize.width / imagesize.width
                    hCanvasMediaRatio = reqImageSize.height / imagesize.height
                    
                    let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
                        var image = request.sourceImage
                        //let blurIntensity = SharedData.SharedInfo.blurIntensity
                        //image = Utils.filterWithGaussianBlur(radius: blurIntensity, imgObject: image, isVideoProcessing: true) as! CIImage
                        
                        if SharedData.SharedInfo.isFiltered {
                            let bpfilter = SharedData.SharedInfo.filterUsed
                            
                            if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                                image = img
                            }
                        }
                        if SharedData.SharedInfo.isTxtEnterd {
                            if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                                let wCIImage = CIImage(cgImage: wcg)
                                image = wCIImage.composited(over: image)
                            }
                        }
                        if let bgimage = backgroundImage, self.generatingFinalVideo {
                            let currentImageHeight = image.extent.size.height * self.hCanvasMediaRatio
                            let bgsize = bgimage.extent.size
                            let translationX = bgsize.width * self.xCanvasMediaRatio
                            /* Reverse calculating height while exporting due to inverse coordinate system */
                            let translationY = bgsize.height - currentImageHeight - (bgsize.height * self.yCanvasMediaRatio)
                            //let xx = bgimage.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                            //let yy = bgimage.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                            
                            image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                            image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                            image = image.composited(over: bgimage)
                        }
                        /*if SharedData.SharedInfo.isBackgroundColorChanged && self.generatingFinalVideo {
                         let xx = colorBackgroundImg.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                         let yy = colorBackgroundImg.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                         
                         image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                         image = image.transformed(by: CGAffineTransform(translationX: xx/2.0, y: yy/2.0))
                         image = image.composited(over: colorBackgroundImg)
                         }*/
                        
                        request.finish(with: image, context: nil)
                    })
                    
                    videoComposition = vidComp
                    
                    let playerItem = AVPlayerItem(asset: avAsset)
                    playerItem.videoComposition = videoComposition
                    videoPlayer?.replaceCurrentItem(with: playerItem)
                    
                } else if SharedData.SharedInfo.isFiltered {
                    let avAsset = AVAsset(url: url)
                    let rectsize = CGRect(origin: CGPoint.zero, size: videoThumbnail?.size ?? CGSize.zero)
                    let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                    let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                    let imagesize = canvasView.frame.size
                    let reqImageSize = videoContainerView.frame.size
                    
                    wCanvasMediaRatio = reqImageSize.width / imagesize.width
                    hCanvasMediaRatio = reqImageSize.height / imagesize.height
                    
                    let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
                        var image = request.sourceImage
                        let bpfilter = SharedData.SharedInfo.filterUsed
                        
                        if let img = Utils.applyFilterOnImage(image: image, type: bpfilter, isVideoProcessing: true) as? CIImage {
                            image = img
                        }
                        if SharedData.SharedInfo.isTxtEnterd {
                            if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                                let wCIImage = CIImage(cgImage: wcg)
                                image = wCIImage.composited(over: image)
                            }
                        }
                        if SharedData.SharedInfo.isBackgroundColorChanged && self.generatingFinalVideo {
                            let currentImageHeight = image.extent.size.height * self.hCanvasMediaRatio
                            let bgsize = colorBackgroundImg.extent.size
                            let translationX = bgsize.width * self.xCanvasMediaRatio
                            /* Reverse calculating height while exporting due to inverse coordinate system */
                            let translationY = bgsize.height - currentImageHeight - (bgsize.height * self.yCanvasMediaRatio)
                            //let xx = colorBackgroundImg.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                            //let yy = colorBackgroundImg.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                            
                            image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                            image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                            image = image.composited(over: colorBackgroundImg)
                        }
                        request.finish(with: image, context: nil)
                    })
                    
                    videoComposition = vidComp
                    
                    let playerItem = AVPlayerItem(asset: avAsset)
                    playerItem.videoComposition = videoComposition
                    videoPlayer?.replaceCurrentItem(with: playerItem)
                    
                } else if SharedData.SharedInfo.isTxtEnterd {
                    let avAsset = AVAsset(url: url)
                    let rectsize = CGRect(origin: CGPoint.zero, size: videoThumbnail?.size ?? CGSize.zero)
                    let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                    let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                    let imagesize = canvasView.frame.size
                    let reqImageSize = videoContainerView.frame.size
                    
                    wCanvasMediaRatio = reqImageSize.width / imagesize.width
                    hCanvasMediaRatio = reqImageSize.height / imagesize.height
                    
                    let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
                        var image = request.sourceImage
                        
                        if let watermark = self.textWatermarkImg, let wcg = watermark.cgImage {
                            let wCIImage = CIImage(cgImage: wcg)
                            image = wCIImage.composited(over: image)
                        }
                        if SharedData.SharedInfo.isBackgroundColorChanged && self.generatingFinalVideo {
                            let currentImageHeight = image.extent.size.height * self.hCanvasMediaRatio
                            let bgsize = colorBackgroundImg.extent.size
                            let translationX = bgsize.width * self.xCanvasMediaRatio
                            /* Reverse calculating height while exporting due to inverse coordinate system */
                            let translationY = bgsize.height - currentImageHeight - (bgsize.height * self.yCanvasMediaRatio)
                            //let xx = colorBackgroundImg.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                            //let yy = colorBackgroundImg.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                            
                            image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                            image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                            image = image.composited(over: colorBackgroundImg)
                        }
                        
                        request.finish(with: image, context: nil)
                    })
                    
                    videoComposition = vidComp
                    
                    let playerItem = AVPlayerItem(asset: avAsset)
                    playerItem.videoComposition = videoComposition
                    videoPlayer?.replaceCurrentItem(with: playerItem)
                    
                } else if SharedData.SharedInfo.isBackgroundColorChanged {
                    let avAsset = AVAsset(url: url)
                    let rectsize = CGRect(origin: CGPoint.zero, size: videoThumbnail?.size ?? CGSize.zero)
                    let ciColor = CIColor(color: SharedData.SharedInfo.backgroundColor)
                    let colorBackgroundImg = Utils.convertToColorImage(color: ciColor, rect: rectsize)
                    let imagesize = canvasView.frame.size
                    let reqImageSize = videoContainerView.frame.size
                    
                    wCanvasMediaRatio = reqImageSize.width / imagesize.width
                    hCanvasMediaRatio = reqImageSize.height / imagesize.height
                    
                    let vidComp = AVMutableVideoComposition(asset: avAsset, applyingCIFiltersWithHandler: { request in
                        var image = request.sourceImage
                        
                        if self.generatingFinalVideo {
                            let currentImageHeight = image.extent.size.height * self.hCanvasMediaRatio
                            let bgsize = colorBackgroundImg.extent.size
                            let translationX = bgsize.width * self.xCanvasMediaRatio
                            /* Reverse calculating height while exporting due to inverse coordinate system */
                            let translationY = bgsize.height - currentImageHeight - (bgsize.height * self.yCanvasMediaRatio)
                            //let xx = colorBackgroundImg.extent.size.width - (image.extent.size.width * self.wCanvasMediaRatio)
                            //let yy = colorBackgroundImg.extent.size.height - (image.extent.size.height * self.hCanvasMediaRatio)
                            
                            image = image.transformed(by: CGAffineTransform(scaleX: self.wCanvasMediaRatio, y: self.hCanvasMediaRatio))
                            image = image.transformed(by: CGAffineTransform(translationX: translationX /*xx/2.0*/, y: translationY /*yy/2.0*/))
                            image = image.composited(over: colorBackgroundImg)
                        }
                        
                        request.finish(with: image, context: nil)
                    })
                    
                    videoComposition = vidComp
                    
                    let playerItem = AVPlayerItem(asset: avAsset)
                    playerItem.videoComposition = videoComposition
                    videoPlayer?.replaceCurrentItem(with: playerItem)
                }
                
                if SharedData.SharedInfo.isCroped {
                    let layerSize = videoContainerView.frame.size
                    
                    videoLayer?.frame = CGRect(x: 0, y: 0, width: layerSize.width, height: layerSize.height)
                    playButton?.center = CGPoint(x: layerSize.width / 2.0, y: layerSize.height / 2.0)
                    SharedData.SharedInfo.isCroped = false
                }
            }
        } catch {
            print(error)
        }
    }
    
    @objc fileprivate func playVideo(_ sender: Any) {
        if videoPlayer?.timeControlStatus == .playing{
            videoPlayer?.pause()
            self.playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
            self.playButton?.isHidden = false
            
        }else if videoPlayer?.timeControlStatus == .paused{
            videoPlayer?.play()
            self.playButton?.setImage(UIImage(named: "pause_btn"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.playButton?.isHidden = true
            }
        }
        //        if let playButton = sender as? UIButton {
        //            if isVideoPlaying {
        //                isVideoPlaying = false
        //                videoPlayer?.pause()
        //                playButton.setImage(UIImage(named: "play_btn"), for: .normal)
        //            } else {
        //                isVideoPlaying = true
        //                videoPlayer?.play()
        //                playButton.setImage(UIImage(named: "pause_btn"), for: .normal)
        //            }
        //        }
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if let player = videoPlayer {
            player.seek(to: .zero)
            player.pause()
        }
        playButton?.isHidden = false
        isVideoPlaying = false
        playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
    }
    
    func pictureCroped(pictData: UIImage) {
        imageContainerView.image = pictData
    }
    
    func resizeCanvas(canvasRatio ratio: String) {
        var availableSize = CGSize.zero
        
        if ratio == "9:16" {
            let availableH = editingView.bounds.size.height
            let availableW = (9.0 * availableH) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "1:1" {
            let availableW = editingView.bounds.size.width
            let availableH = (1.0 * availableW) / 1.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:5" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 5.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "16:9" {
            let availableW = editingView.bounds.size.width
            let availableH = (9.0 * availableW) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "2:3" {
            let availableH = editingView.bounds.size.height
            let availableW = (2.0 * availableH) / 3.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "3:4" {
            let availableH = editingView.bounds.size.height
            let availableW = (3.0 * availableH) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:3" {
            let availableW = editingView.bounds.size.width
            let availableH = (3.0 * availableW) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:6" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:4" {
            let availableW = editingView.bounds.size.width
            let availableH = (4.0 * availableW) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:7" {
            let availableH = editingView.bounds.size.height
            let availableW = (6.0 * availableH) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "7:6" {
            let availableW = editingView.bounds.size.width
            let availableH = (6.0 * availableW) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "8:10" {
            let availableH = editingView.bounds.size.height
            let availableW = (8.0 * availableH) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "10:8" {
            let availableW = editingView.bounds.size.width
            let availableH = (8.0 * availableW) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            let canvasSize = Utils.getFitFrameSize(maxSize: self.editingView.bounds.size, actualSize: availableSize)
            
            self.canvasWidthConstraint.constant = canvasSize.width
            self.canvasHeightConstraint.constant = canvasSize.height
            self.canvasView.frame = CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height)
            self.canvasView.center = CGPoint(x: self.editingView.frame.size.width / 2.0, y: self.editingView.frame.size.height / 2.0)
            self.canvasView.layoutIfNeeded()
            
            if self.mediaType == .photo {
                let actualSize = self.imageContainerView.image?.size ?? CGSize.zero
                self.imageContainerView.frame.size = Utils.getFitFrameSize(maxSize: canvasSize, actualSize: actualSize)
                
                if self.canvasResized {
                    self.canvasResized = false
                    self.imageContainerView.center = CGPoint(x: canvasSize.width / 2.0, y: canvasSize.height / 2.0)
                }
            } else if self.mediaType == .video {
                if let vidUrl = self.videoURL {
                    let asset = AVURLAsset(url: vidUrl, options: nil)
                    
                    if let track = asset.tracks(withMediaType: AVMediaType.video).first {
                        let mSize = __CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform)
                        let actualSize = CGSize(width: fabs(Double(mSize.width)), height: fabs(Double(mSize.height)))
                        let layerSize = Utils.getFitFrameSize(maxSize: canvasSize, actualSize: actualSize)
                        
                        self.videoContainerView.frame.size = layerSize
                        
                        if self.canvasResized {
                            self.canvasResized = false
                            self.videoContainerView.center = CGPoint(x: canvasSize.width / 2.0, y: canvasSize.height / 2.0)
                        }
                        
                        //self.videoLayer?.frame = CGRect(x: 0, y: 0, width: layerSize.width, height: layerSize.height)
                        self.playButton?.center = CGPoint(x: layerSize.width / 2.0, y: layerSize.height / 2.0)
                    }
                }
            }
        }) { (completion) in
            if self.mediaType == .video {
                let vidConSize = self.videoContainerView.frame.size
                self.videoLayer?.frame = CGRect(x: 0, y: 0, width: vidConSize.width, height: vidConSize.height)
            }
        }
    }
}

extension MainVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == canvasSizeCollectionView{
            return nameArr.count
        }
        return bottomBarIcons.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == canvasSizeCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCanvas", for: indexPath) as! VideoCanvasCVCell
            cell.image.image = photoImages[indexPath.row]
            cell.lbl.text = nameArr[indexPath.row]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        cell.lbl.text  = bottomBarLabels[indexPath.item]
        cell.img.image = bottomBarIcons[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == canvasSizeCollectionView {
            selectedCanvasRatio = nameArr[indexPath.row]
            canvasResized = true
            resizeCanvas(canvasRatio: selectedCanvasRatio)
        } else {
            if let player = videoPlayer {
                player.seek(to: .zero)
                player.pause()
            }
            
            isVideoPlaying = false
            playButton?.setImage(UIImage(named: "play_btn"), for: .normal)
            
            if indexPath.item == 0 {
                canvasToolBarView.isHidden = false
                
                /*let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CanvasVC") as? CanvasVC
                 
                 if mediaType == .video {
                 vc?.image = videoThumbnail//Utils.getVideoThumbnail(from: videoURL!)
                 vc?.mediaType = .video
                 vc?.videoThumbnail = videoThumbnail//Utils.getVideoThumbnail(from: videoURL!)
                 vc?.videoUrl = videoURL
                 vc?.textWatermarkImg = textWatermarkImg
                 } else {
                 vc?.mediaType = .photo
                 if SharedData.SharedInfo.isEditable == true {
                 vc?.image = SharedData.SharedInfo.globalEditedImage
                 } else {
                 vc?.image = myImg
                 }
                 
                 vc?.delegate = vc
                 }
                 
                 self.navigationController?.pushViewController(vc!, animated: false)*/
            } else if indexPath.item == 1 {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ColorVC") as? ColorVC
                var image: UIImage? = nil
                var mcCenter: CGPoint? = CGPoint.zero
                var mcTransform: CGAffineTransform? = nil
                
                if mediaType == .photo {
                    let isEditable = SharedData.SharedInfo.isEditable
                    image = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
                    mcTransform = imageContainerView.transform
                    mcCenter = imageContainerView.center
                } else {
                    SharedData.SharedInfo.isEditable = false
                    image = videoThumbnail
                    mcTransform = videoContainerView.transform
                    mcCenter = videoContainerView.center
                }
                
                vc?.mcTransform = mcTransform
                vc?.mcCenter = mcCenter
                vc?.myImg = image ?? UIImage()
                self.navigationController?.pushViewController(vc!, animated: false)
            } else if indexPath.item == 2 {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BlurVC") as? BlurVC
                var image: UIImage? = nil
                var mcCenter: CGPoint? = CGPoint.zero
                var mcTransform: CGAffineTransform? = nil
                
                if mediaType == .photo {
                    let isEditable = SharedData.SharedInfo.isEditable
                    image = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
                    mcTransform = imageContainerView.transform
                    mcCenter = imageContainerView.center
                } else {
                    SharedData.SharedInfo.isEditable = false
                    image = videoThumbnail
                    mcTransform = videoContainerView.transform
                    mcCenter = videoContainerView.center
                }
                
                vc?.selectedCanvasRatio = selectedCanvasRatio
                vc?.mcTransform = mcTransform
                vc?.mcCenter = mcCenter
                vc?.myImg = image ?? UIImage()
                self.navigationController?.pushViewController(vc!, animated: false)
            } else if indexPath.item == 3 {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterVC") as? FilterVC
                var image: UIImage? = nil
                var mcTransform: CGAffineTransform? = nil
                var mcCenter: CGPoint? = CGPoint.zero
                
                if mediaType == .photo {
                    let isEditable = SharedData.SharedInfo.isEditable
                    image = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
                    mcTransform = imageContainerView.transform
                    mcCenter = imageContainerView.center
                } else {
                    SharedData.SharedInfo.isEditable = false
                    image = videoThumbnail
                    mcTransform = videoContainerView.transform
                    mcCenter = videoContainerView.center
                }
                
                vc?.mcTransform = mcTransform
                vc?.mcCenter = mcCenter
                vc?.myImg = image ?? UIImage()
                self.navigationController?.pushViewController(vc!, animated: false)
            } else if indexPath.item == 4 {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TextVC") as? TextVC
                var image: UIImage? = nil
                var mcTransform: CGAffineTransform? = nil
                var textImg: UIImage? = nil
                var mcCenter: CGPoint? = CGPoint.zero
                
                SharedData.SharedInfo.isTxtEnterd = false
                
                if mediaType == .photo {
                    let isEditable = SharedData.SharedInfo.isEditable
                    image = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
                    mcTransform = imageContainerView.transform
                    mcCenter = imageContainerView.center
                } else {
                    SharedData.SharedInfo.updateTextImageLayer = false
                    SharedData.SharedInfo.isEditable = false
                    image = videoThumbnail
                    mcTransform = videoContainerView.transform
                    textImg = textWatermarkImg
                    mcCenter = videoContainerView.center
                }
                
                vc?.textWatermarkImg = textImg
                vc?.mcTransform = mcTransform
                vc?.mcCenter = mcCenter
                vc?.mediaType = mediaType
                vc?.myImg = image ?? UIImage()
                
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
    }
}

extension MainVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == canvasSizeCollectionView{
            return 25
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/5.8
        return CGSize(width: yourWidth, height: collectionView.bounds.size.height)
    }
}

// MARK:- UICircularProgressRingDelegate

extension MainVC:UICircularProgressRingDelegate{
    func didFinishProgress(for ring: UICircularProgressRing) {
        
    }
    
    func didPauseProgress(for ring: UICircularProgressRing) {
        
    }
    
    func didContinueProgress(for ring: UICircularProgressRing) {
        
    }
    
    func didUpdateProgressValue(for ring: UICircularProgressRing, to newValue: CGFloat) {
        
    }
    
    func willDisplayLabel(for ring: UICircularProgressRing, _ label: UILabel) {
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 40)//UIFont.systemFont(ofSize: 25.0)
    }
    
}

