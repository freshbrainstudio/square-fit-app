//
//  BlurVC.swift
//  SquareFit
//
//  Created by Abdul Qadar on 05/09/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit

class BlurVC: UIViewController {
    
    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var imageContainerView: UIImageView!
    @IBOutlet weak var canvasWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var slider: UISlider!
    
    var myImg = UIImage()
    var blrIMG: UIImage? = nil
    var mcTransform: CGAffineTransform? = nil
    var mcCenter: CGPoint? = nil
    var isBlurNone = false
    var selectedCanvasRatio = "1:1"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        slider.value = SharedData.SharedInfo.blurIntensity
        slider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if SharedData.SharedInfo.isBackgroundColorChanged {
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
            videoContainerView.isHidden = true
        }

        let isEditable = SharedData.SharedInfo.isEditable
        let img = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
        
        imageContainerView.image = img
        canvasView.layer.contents = img.cgImage
        canvasView.layer.contentsGravity = .resizeAspectFill

        perform(#selector(resizeCanvas), with: nil, afterDelay: 0.0)
        //perform(#selector(adjustImageviewFrame), with: nil, afterDelay: 0.0)
    }
    
    // MARK: - IBActions
    
    @IBAction func sliderChange(_ slider: UISlider) {
        print(slider.value)
    }
    
    @IBAction func actionBlur(_ sender: Any) {
        blrIMG = SharedData.SharedInfo.isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
        slider.value = 50.0
        isBlurNone = false
        
        if let img = blrIMG {
            let uiimage = Utils.filterWithGaussianBlur(radius: self.slider.value, imgObject: img, isVideoProcessing: false) as? UIImage
            
            canvasView.layer.contents = uiimage?.cgImage
            canvasView.backgroundColor = UIColor.clear
            //imageContainerView.image = Utils.filterWithGaussianBlur(radius: self.slider.value, imgObject: img, isVideoProcessing: false) as? UIImage
        }
    }
    
    @IBAction func actionNoBlur(_ sender: Any) {
        blrIMG = SharedData.SharedInfo.isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
        slider.value = 0.0
        isBlurNone = false

        if let _ = blrIMG {
            canvasView.layer.contents = blrIMG?.cgImage
            canvasView.backgroundColor = UIColor.clear
        }
        
        /*if let img = blrIMG {
            imageContainerView.image = img
        }*/
    }
     
    @IBAction func actionBlurNone(_ sender: Any) {
        canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
        canvasView.layer.contents = nil
        slider.value = 0.0
        isBlurNone = true
        
        /*if let img = blrIMG {
            imageContainerView.image = img
        }*/
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        if let _ = imageContainerView.image {
            SharedData.SharedInfo.isBackgroundColorChanged = isBlurNone
            
            if isBlurNone == false {
                SharedData.SharedInfo.backgroundColor = UIColor.white
                SharedData.SharedInfo.isBlurred = true
            }
            SharedData.SharedInfo.blurIntensity = self.slider.value
            //SharedData.SharedInfo.globalEditedImage = img
            //SharedData.SharedInfo.isEditable = true
            SharedData.SharedInfo.isEditingStarted = true
        }
        
        popVC()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        popVC()
    }
    
    // MARK : - Custom Functions
        
    @objc func adjustImageviewFrame() {
        let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: imageContainerView.image?.size ?? CGSize.zero)
        
        canvasWidthConstraint.constant = size.width
        canvasHeightConstraint.constant = size.height
        imageContainerView.transform = mcTransform ?? imageContainerView.transform
            
        perform(#selector(reCenter), with: nil, afterDelay: 0.0)
    }
            
    @objc func reCenter() {
        imageContainerView.center = mcCenter ?? imageContainerView.center
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            let alert = AlertView.prepare(title: "Save error", message:"There is some error while saving edited picture to gallery.", okAction: nil)
            self.present(alert, animated: true, completion: nil)
        } else {
            
            let alert = AlertView.prepare(title: "Saved!", message: "Your image has been saved to your photos.") {
                SharedData.SharedInfo.globalEditedImage = self.canvasView.asImage()//self.renderImg
                SharedData.SharedInfo.isEditable = true
                self.popVC()
            }
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                print("began")
                // handle drag began
            case .moved:
                print("moved")
            case .ended:
                let isEditable = SharedData.SharedInfo.isEditable
                blrIMG = isEditable ? SharedData.SharedInfo.globalEditedImage : self.myImg
                
                if let img = blrIMG {
                    let uiimage = Utils.filterWithGaussianBlur(radius: self.slider.value, imgObject: img, isVideoProcessing: false) as? UIImage
                    
                    canvasView.layer.contents = uiimage?.cgImage
                    canvasView.backgroundColor = UIColor.clear
                    
                    //imageContainerView.image = Utils.filterWithGaussianBlur(radius: self.slider.value, imgObject: img, isVideoProcessing: false) as? UIImage
                }
                print("ended")
            default:
                break
            }
        }
    }
    
    @objc func resizeCanvas() {
        var availableSize = CGSize.zero
        let ratio = selectedCanvasRatio
        
        if ratio == "9:16" {
            let availableH = editingView.bounds.size.height
            let availableW = (9.0 * availableH) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "1:1" {
            let availableW = editingView.bounds.size.width
            let availableH = (1.0 * availableW) / 1.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:5" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 5.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "16:9" {
            let availableW = editingView.bounds.size.width
            let availableH = (9.0 * availableW) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "2:3" {
            let availableH = editingView.bounds.size.height
            let availableW = (2.0 * availableH) / 3.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "3:4" {
            let availableH = editingView.bounds.size.height
            let availableW = (3.0 * availableH) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:3" {
            let availableW = editingView.bounds.size.width
            let availableH = (3.0 * availableW) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:6" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:4" {
            let availableW = editingView.bounds.size.width
            let availableH = (4.0 * availableW) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:7" {
            let availableH = editingView.bounds.size.height
            let availableW = (6.0 * availableH) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "7:6" {
            let availableW = editingView.bounds.size.width
            let availableH = (6.0 * availableW) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "8:10" {
            let availableH = editingView.bounds.size.height
            let availableW = (8.0 * availableH) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "10:8" {
            let availableW = editingView.bounds.size.width
            let availableH = (8.0 * availableW) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        }
        
        let canvasSize = Utils.getFitFrameSize(maxSize: self.editingView.bounds.size, actualSize: availableSize)
        
        self.canvasWidthConstraint.constant = canvasSize.width
        self.canvasHeightConstraint.constant = canvasSize.height
        self.canvasView.frame = CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height)
        self.canvasView.center = CGPoint(x: self.editingView.frame.size.width / 2.0, y: self.editingView.frame.size.height / 2.0)
        self.canvasView.layoutIfNeeded()
                
        let actualSize = self.imageContainerView.image?.size ?? CGSize.zero
        
        self.imageContainerView.frame.size = Utils.getFitFrameSize(maxSize: canvasSize, actualSize: actualSize)
        //self.imageContainerView.center = CGPoint(x: canvasSize.width / 2.0, y: canvasSize.height / 2.0)
        self.imageContainerView.transform = mcTransform ?? imageContainerView.transform
        self.imageContainerView.center = mcCenter ?? imageContainerView.center
    }
}
