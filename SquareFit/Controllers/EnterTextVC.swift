//
//  EnterTextVC.swift
//  SquareFit
//
//  Created by Fresh Brain on 17/09/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit

class EnterTextVC: UIViewController {

    
    @IBOutlet weak var txtView: UITextView! {
        didSet {
            self.txtView.becomeFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        SharedData.SharedInfo.isTxtEnterd = false
        self.dismissVC(completion: nil)
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        if validate(textView: txtView){
            SharedData.SharedInfo.enteredTxt = self.txtView.text
            SharedData.SharedInfo.isTxtEnterd = true
        }else{
            print("Empty")
        }
        self.txtView.resignFirstResponder()
        self.dismissKeyboard()
        self.dismissVC(completion: nil)
    }
    func validate(textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            return false
        }

        return true
    }

}

extension EnterTextVC : UITextViewDelegate  {
    
    
}
