//
//  FilterVC.swift
//  SquareFit
//
//  Created by Abdul Qadar on 05/09/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {

    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var imageContainerView: UIImageView!
    @IBOutlet weak var canvasWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collection: UICollectionView!

    var myImg = UIImage()
    var allFilterNames = Constants.bpFilterNames
    var allFiltersAvailable = Constants.bpFilters
    var selectedIndexPath = IndexPath()
    var mcTransform: CGAffineTransform? = nil
    var mcCenter: CGPoint? = nil        
    var selectedCanvasRatio = "1:1"
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let isEditable = SharedData.SharedInfo.isEditable
        let img = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
        
        if SharedData.SharedInfo.isBackgroundColorChanged {
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
            canvasView.layer.contents = nil
        }
        if SharedData.SharedInfo.isBlurred {
            let intensity = SharedData.SharedInfo.blurIntensity
            let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: img, isVideoProcessing: false) as? UIImage
            
            canvasView.backgroundColor = UIColor.clear
            canvasView.layer.contents = uiimage?.cgImage
            videoContainerView.isHidden = true
        }
        
        imageContainerView.image = img
                
        self.collection.reloadData()
        perform(#selector(resizeCanvas), with: nil, afterDelay: 0.0)
        //perform(#selector(adjustImageviewFrame), with: nil, afterDelay: 0.0)
    }
    
    @objc func adjustImageviewFrame() {
        let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: imageContainerView.image?.size ?? CGSize.zero)
        canvasWidthConstraint.constant = size.width
        canvasHeightConstraint.constant = size.height
        
        if SharedData.SharedInfo.isFiltered{
            let indexPath = SharedData.SharedInfo.selectedIndexPathForFilters
            collection.selectItem(at: indexPath, animated: true, scrollPosition: [.centeredVertically, .centeredHorizontally])
        }
                
        imageContainerView.transform = mcTransform ?? imageContainerView.transform
        perform(#selector(reCenter), with: nil, afterDelay: 0.0)
    }
        
    @objc func reCenter() {
        imageContainerView.center = mcCenter ?? imageContainerView.center
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            let alert = AlertView.prepare(title: "Save error", message:"There is some error while saving edited picture to gallery.", okAction: nil)
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = AlertView.prepare(title: "Saved!", message: "Your image has been saved to your photos.") {
                SharedData.SharedInfo.globalEditedImage = self.imageContainerView.asImage()
                SharedData.SharedInfo.isEditable = true
                self.popVC()
            }
            SharedData.SharedInfo.globalEditedImage = imageContainerView.asImage()
            SharedData.SharedInfo.isEditable = true
            
            self.popVC()
            self.present(alert, animated: true, completion: nil)
        }
    }

    @objc func resizeCanvas() {
        var availableSize = CGSize.zero
        let ratio = selectedCanvasRatio
        
        if ratio == "9:16" {
            let availableH = editingView.bounds.size.height
            let availableW = (9.0 * availableH) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "1:1" {
            let availableW = editingView.bounds.size.width
            let availableH = (1.0 * availableW) / 1.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:5" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 5.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "16:9" {
            let availableW = editingView.bounds.size.width
            let availableH = (9.0 * availableW) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "2:3" {
            let availableH = editingView.bounds.size.height
            let availableW = (2.0 * availableH) / 3.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "3:4" {
            let availableH = editingView.bounds.size.height
            let availableW = (3.0 * availableH) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:3" {
            let availableW = editingView.bounds.size.width
            let availableH = (3.0 * availableW) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:6" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:4" {
            let availableW = editingView.bounds.size.width
            let availableH = (4.0 * availableW) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:7" {
            let availableH = editingView.bounds.size.height
            let availableW = (6.0 * availableH) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "7:6" {
            let availableW = editingView.bounds.size.width
            let availableH = (6.0 * availableW) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "8:10" {
            let availableH = editingView.bounds.size.height
            let availableW = (8.0 * availableH) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "10:8" {
            let availableW = editingView.bounds.size.width
            let availableH = (8.0 * availableW) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        }
        
        let canvasSize = Utils.getFitFrameSize(maxSize: self.editingView.bounds.size, actualSize: availableSize)
        
        self.canvasWidthConstraint.constant = canvasSize.width
        self.canvasHeightConstraint.constant = canvasSize.height
        self.canvasView.frame = CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height)
        self.canvasView.center = CGPoint(x: self.editingView.frame.size.width / 2.0, y: self.editingView.frame.size.height / 2.0)
        self.canvasView.layoutIfNeeded()
                
        let actualSize = self.imageContainerView.image?.size ?? CGSize.zero
        
        self.imageContainerView.frame.size = Utils.getFitFrameSize(maxSize: canvasSize, actualSize: actualSize)
        //self.imageContainerView.center = CGPoint(x: canvasSize.width / 2.0, y: canvasSize.height / 2.0)
        self.imageContainerView.transform = mcTransform ?? imageContainerView.transform
        self.imageContainerView.center = mcCenter ?? imageContainerView.center
    }
    
    // MARK: - IBActions
    
    @IBAction func actionBack(_ sender: UIButton) {
//        SharedData.SharedInfo.isFiltered = false
//        SharedData.SharedInfo.filterUsed = BPFilter.none
        
        self.popVC()
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        if let image = imageContainerView.image {
            SharedData.SharedInfo.globalEditedImage = image
            SharedData.SharedInfo.isEditable = true
            SharedData.SharedInfo.isEditingStarted = true
            SharedData.SharedInfo.selectedIndexPathForFilters = selectedIndexPath
            let filter = allFiltersAvailable[selectedIndexPath.row]
            SharedData.SharedInfo.filterUsed = filter
            SharedData.SharedInfo.isFiltered = true
            self.popVC()
        }
    }
}


extension FilterVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allFilterNames.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        let backgroundSelectionView = UIView()
        backgroundSelectionView.bounds = cell.frame
        backgroundSelectionView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundSelectionView
        let width = cell.img.frame.width
        let height = cell.img.frame.height
        let filter = allFiltersAvailable[indexPath.row]
        cell.lbl.text = allFilterNames[indexPath.item]
        
        let thumb = self.getImageThumbnail(image: self.myImg,height: height,width: width)
        cell.img.image = Utils.applyFilterOnImage(image: thumb, type: filter, isVideoProcessing: false) as? UIImage
        
        return cell
    }
    func getImageThumbnail(image:UIImage,height:CGFloat,width:CGFloat) ->UIImage{
        let tempImage               = Utils.cropToSquareBounds(image: image)
        let aspectRatio             = tempImage.size.width / tempImage.size.height
        let thumbImageSize          = CGSize(width: aspectRatio * width, height:height)
        let thumbImage              = Utils.scaleImage(image: tempImage, toSize: thumbImageSize)
        return thumbImage
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        let filter = allFiltersAvailable[indexPath.row]
        imageContainerView.image = Utils.applyFilterOnImage(image: myImg, type: filter, isVideoProcessing: false) as? UIImage
    }
}
extension FilterVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/7.0
        let yourHeight = collectionView.bounds.width/5.5
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
