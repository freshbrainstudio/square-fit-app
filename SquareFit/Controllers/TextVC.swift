//
//  TextVC.swift
//  SquareFit
//
//  Created by Abdul Qadar on 05/09/2019.
//  Copyright © 2019 Ahmad Mustafa. All rights reserved.
//

import UIKit
import FlexColorPicker


class TextVC: UIViewController {
    
    var selectedFontIndexPath = IndexPath()
    var selectedFontName = UIFont(name: "Asimov", size: 25)
    var selectedFontColor:UIColor = UIColor.white
    
    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var imageContainerView: JLStickerImageView!
    @IBOutlet weak var canvasWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var canvasHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var fontView: UIView!
    @IBOutlet weak var fontCollection: UICollectionView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var colorHideBtn: UIButton!
    @IBOutlet open var rectangularHsbPalette: RectangularPaletteControl? {
        get {
            return colorPicker.rectangularHsbPalette
        }
        set {
            colorPicker.rectangularHsbPalette = newValue
        }
    }

    open var delegate: ColorPickerDelegate? {
        get {
            return colorPicker.delegate
        }
        set {
            colorPicker.delegate = newValue
        }
    }
    
    var textWatermarkImg: UIImage? = nil
    var myImg = UIImage()
    var mediaType = MediaType.photo
    let colorPicker = ColorPickerController()
    var fontNamesArray = ["Asimov", "Aspire-DemiBold", "BroshK", "Bungasai", "CaviarDreams", "JennaSua", "Kirvy-Bold",
                          "Limelight", "Lobster1.4", "Namskout", "Parisish", "PoiretOne-Regular", "RegencieLight", "Rochester-Regular",
                          "Rudiment", "Snickles", "timeburnernormal", "trench100free", "Variane Script", "WeAreInLove",
                          "Monument_Valley_1.2-Regular", "NORTHWEST-B-DEMO", "CroissantOne-Regular"]
    var mcTransform: CGAffineTransform? = nil
    var mcCenter: CGPoint? = nil
    var selectedCanvasRatio = "1:1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fontCollection.delegate = self
        fontCollection.dataSource = self
        imageContainerView.mediaType = mediaType
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let indexPath = IndexPath(item: 0, section: 0)
        fontCollection.selectItem(at: indexPath, animated: true, scrollPosition: [.centeredVertically, .centeredHorizontally])
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let isEditable = SharedData.SharedInfo.isEditable
        let img = isEditable ? SharedData.SharedInfo.globalEditedImage : myImg
        
        if SharedData.SharedInfo.isTxtEnterd == true {
            self.setUpCustomTextView(txt: SharedData.SharedInfo.enteredTxt, font: selectedFontName ?? UIFont.systemFont(ofSize: 25.0))
            rectangularHsbPalette?.selectedColor = UIColor.white
            rectangularHsbPalette?.setSelectedHSBColor(UIColor.white.hsbColor, isInteractive: true)
        }
        if SharedData.SharedInfo.isBackgroundColorChanged {
            canvasView.backgroundColor = SharedData.SharedInfo.backgroundColor
            canvasView.layer.contents = nil
        }
        if SharedData.SharedInfo.isBlurred {
            let intensity = SharedData.SharedInfo.blurIntensity
            let uiimage = Utils.filterWithGaussianBlur(radius: intensity, imgObject: img, isVideoProcessing: false) as? UIImage
            
            canvasView.backgroundColor = UIColor.clear
            canvasView.layer.contents = uiimage?.cgImage
        }

        if mediaType == .video  && textWatermarkImg != nil {
            UIGraphicsBeginImageContextWithOptions(img.size, false, 0)
            img.draw(in: CGRect(origin: CGPoint.zero, size: img.size))
            textWatermarkImg?.draw(in: CGRect(origin: CGPoint.zero, size: img.size))
            
            let outImg = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            imageContainerView.image = outImg
        } else {
            imageContainerView.image = img
        }
        
        videoContainerView.isHidden = true
        
        perform(#selector(resizeCanvas), with: nil, afterDelay: 0.0)
        //perform(#selector(adjustImageviewFrame), with: nil, afterDelay: 0.0)
    }
    
    @objc func adjustImageviewFrame() {
        let size = Utils.getFitFrameSize(maxSize: editingView.bounds.size, actualSize: imageContainerView.image?.size ?? CGSize.zero)
        
        canvasWidthConstraint.constant = size.width
        canvasHeightConstraint.constant = size.height
        imageContainerView.transform = mcTransform ?? imageContainerView.transform
            
        perform(#selector(reCenter), with: nil, afterDelay: 0.0)
    }
        
    @objc func reCenter() {
        imageContainerView.center = mcCenter ?? imageContainerView.center
    }

    @objc  func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    func setUpCustomTextView(txt: String,font:UIFont) {
        imageContainerView.addLabel()
        imageContainerView.currentlyEditingLabel.labelTextView!.text = txt
        imageContainerView.currentlyEditingLabel.labelTextView!.isUserInteractionEnabled = false
        imageContainerView.textColor = .white
        imageContainerView.textAlpha = 1
        imageContainerView.currentlyEditingLabel.closeView!.image = UIImage(named: "x")
        imageContainerView.currentlyEditingLabel.rotateView?.image = UIImage(named: "res")
        imageContainerView.currentlyEditingLabel.border?.strokeColor = UIColor.white.cgColor
        imageContainerView.currentlyEditingLabel.labelTextView?.font = font
        imageContainerView.adjustFontSizeToFillRect(imageContainerView.currentlyEditingLabel.bounds, view: imageContainerView.currentlyEditingLabel)
        imageContainerView.adjustsWidthToFillItsContens(imageContainerView.currentlyEditingLabel)
        imageContainerView.currentlyEditingLabel.delegate = self

    }
    
    @objc func resizeCanvas() {
        var availableSize = CGSize.zero
        let ratio = selectedCanvasRatio
        
        if ratio == "9:16" {
            let availableH = editingView.bounds.size.height
            let availableW = (9.0 * availableH) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "1:1" {
            let availableW = editingView.bounds.size.width
            let availableH = (1.0 * availableW) / 1.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:5" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 5.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "16:9" {
            let availableW = editingView.bounds.size.width
            let availableH = (9.0 * availableW) / 16.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "2:3" {
            let availableH = editingView.bounds.size.height
            let availableW = (2.0 * availableH) / 3.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "3:4" {
            let availableH = editingView.bounds.size.height
            let availableW = (3.0 * availableH) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:3" {
            let availableW = editingView.bounds.size.width
            let availableH = (3.0 * availableW) / 4.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "4:6" {
            let availableH = editingView.bounds.size.height
            let availableW = (4.0 * availableH) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:4" {
            let availableW = editingView.bounds.size.width
            let availableH = (4.0 * availableW) / 6.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "6:7" {
            let availableH = editingView.bounds.size.height
            let availableW = (6.0 * availableH) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "7:6" {
            let availableW = editingView.bounds.size.width
            let availableH = (6.0 * availableW) / 7.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "8:10" {
            let availableH = editingView.bounds.size.height
            let availableW = (8.0 * availableH) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        } else if ratio == "10:8" {
            let availableW = editingView.bounds.size.width
            let availableH = (8.0 * availableW) / 10.0
            
            availableSize = CGSize(width: availableW, height: availableH)
        }
        
        let canvasSize = Utils.getFitFrameSize(maxSize: self.editingView.bounds.size, actualSize: availableSize)
        
        self.canvasWidthConstraint.constant = canvasSize.width
        self.canvasHeightConstraint.constant = canvasSize.height
        self.canvasView.frame = CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height)
        self.canvasView.center = CGPoint(x: self.editingView.frame.size.width / 2.0, y: self.editingView.frame.size.height / 2.0)
        self.canvasView.layoutIfNeeded()
                
        let actualSize = self.imageContainerView.image?.size ?? CGSize.zero
        
        self.imageContainerView.frame.size = Utils.getFitFrameSize(maxSize: canvasSize, actualSize: actualSize)
        //self.imageContainerView.center = CGPoint(x: canvasSize.width / 2.0, y: canvasSize.height / 2.0)
        self.imageContainerView.transform = mcTransform ?? imageContainerView.transform
        self.imageContainerView.center = mcCenter ?? imageContainerView.center
    }

    // MARK: - IBActions
    
    @IBAction func actionBack(_ sender: UIButton) {
        if let oldtextimage = textWatermarkImg {
            SharedData.SharedInfo.globalEditedImage = oldtextimage
            SharedData.SharedInfo.isTxtEnterd = true
            SharedData.SharedInfo.isEditable = true
            SharedData.SharedInfo.isEditingStarted = true
            SharedData.SharedInfo.updateTextImageLayer = true
        }
        
        popVC()
    }
    
    @IBAction func actionSave(_ sender: UIButton) {
        imageContainerView.cleanup()
        
        let renderImg = imageContainerView.renderContentOnView(overlap: textWatermarkImg)
        SharedData.SharedInfo.globalEditedImage = renderImg ?? UIImage.init()
        SharedData.SharedInfo.isEditable = true
        SharedData.SharedInfo.isEditingStarted = true
        SharedData.SharedInfo.updateTextImageLayer = true
        
        popVC()
    }
    
    @IBAction func actionAddFont(_ sender: Any) {
        self.fontView.setUpView(view: fontView, hidden: false)
    }
    
    @IBAction func actionAddColor(_ sender: Any) {
        self.colorPicker.delegate = self
        self.colorView.setUpView(view: colorView, hidden: false)
    }
    
    @IBAction func actionAddText(_ sender: Any) {
        self.presentWithIdentifier(name: "EnterTextVC")
    }
    
    @IBAction func actionColorViewHide(_ sender: Any) {
        self.colorView.setDownView(view: colorView, hidden: true)
    }
    
    @IBAction func actionFontViewHide(_ sender: Any) {
        self.fontCollection.delegate = self
        self.fontCollection.dataSource = self
        self.fontView.setDownView(view: fontView, hidden: true)
       
    }
}

extension TextVC : ColorPickerDelegate {
    
    func colorPicker(_ colorPicker: ColorPickerController, confirmedColor selectedColor: UIColor, usingControl: ColorControl) {
        
    }
    @objc func colorChangeConfirmed(){
        //imageContainerView.currentlyEditingLabel.labelTextView?.font = selectedFontName
        //imageContainerView.adjustFontSizeToFillRect(imageContainerView.bounds, view: imageContainerView.currentlyEditingLabel)
    }

    func colorPicker(_ colorPicker: ColorPickerController, selectedColor: UIColor, usingControl: ColorControl) {
        imageContainerView.textColor = selectedColor
        selectedFontColor = selectedColor
        usingControl.addTarget(self, action: #selector(colorChangeConfirmed), for: .valueChanged)
    }
}


extension TextVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fontNamesArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FontCollectionCell", for: indexPath) as! FontCollectionCell
        DispatchQueue.main.async {
            cell.lbl.font = UIFont(name: "\(self.fontNamesArray[indexPath.item])", size: 25)
        }
        let backgroundSelectionView = UIView()
        backgroundSelectionView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundSelectionView
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageContainerView.fontName = self.fontNamesArray[indexPath.item]
         if SharedData.SharedInfo.isTxtEnterd == true {
        let fontSize = imageContainerView.currentlyEditingLabel.labelTextView?.fontSize
        selectedFontName = UIFont(name: "\(self.fontNamesArray[indexPath.item])", size: fontSize ?? 25) ?? UIFont(name: "Asimov", size: fontSize ?? 25)
        imageContainerView.currentlyEditingLabel.labelTextView?.font = selectedFontName
        imageContainerView.adjustsWidthToFillItsContens(imageContainerView.currentlyEditingLabel)
        }
    }
    
}

extension TextVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
}

extension TextVC:JLStickerLabelViewDelegate{
    
    func labelViewDidSelected(_ label: JLStickerLabelView) {
        print("Did select")
        rectangularHsbPalette?.selectedColor = label.labelTextView?.textColor ?? UIColor.white
        imageContainerView.currentlyEditingLabel = label
        rectangularHsbPalette?.setSelectedHSBColor(label.labelTextView?.textColor?.hsbColor ?? UIColor.white.hsbColor, isInteractive: true)

    }
    func labelViewDidBeginEditing(_ label: JLStickerLabelView) {
        print("Did Begin")
        rectangularHsbPalette?.selectedColor = label.labelTextView?.textColor ?? UIColor.white
        imageContainerView.currentlyEditingLabel = label
        rectangularHsbPalette?.setSelectedHSBColor(label.labelTextView?.textColor?.hsbColor ?? UIColor.white.hsbColor, isInteractive: true)
//        rectangularHsbPalette?.setSelectedHSBColor(UIColor.white.hsbColor, isInteractive: true)
        
    }
    
    func labelViewDidClose(_ label: JLStickerLabelView) {

    }
    
    func labelViewDidShowEditingHandles(_ label: JLStickerLabelView) {
        
    }
    
    func labelViewDidHideEditingHandles(_ label: JLStickerLabelView) {
    }
    
    func labelViewDidStartEditing(_ label: JLStickerLabelView) {
        
    }
    
    func labelViewDidChangeEditing(_ label: JLStickerLabelView) {
        
    }
    
    func labelViewDidEndEditing(_ label: JLStickerLabelView) {
    }
}
