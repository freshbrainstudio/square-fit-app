//
//  Utils.swift
//  BlurPhoto
//
//  Created by Qaiser Butt on 3/24/18.
//  Copyright © 2018 Red Owl. All rights reserved.
//

import Foundation
import UIKit
import Photos

class Utils {
    static func rotationTransform() -> CGAffineTransform {
      switch UIDevice.current.orientation {
      case .landscapeLeft:
        return CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
      case .landscapeRight:
        return CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
      case .portraitUpsideDown:
        return CGAffineTransform(rotationAngle: CGFloat(Double.pi))
      default:
        return CGAffineTransform.identity
      }
    }

    static func videoOrientation() -> AVCaptureVideoOrientation {
      switch UIDevice.current.orientation {
      case .portrait:
        return .portrait
      case .landscapeLeft:
        return .landscapeRight
      case .landscapeRight:
        return .landscapeLeft
      case .portraitUpsideDown:
        return .portraitUpsideDown
      default:
        return .portrait
      }
    }

    static func fetchOptions() -> PHFetchOptions {
      let options = PHFetchOptions()
      options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]

      return options
    }

    static func format(_ duration: TimeInterval) -> String {
      let formatter = DateComponentsFormatter()
      formatter.zeroFormattingBehavior = .pad

      if duration >= 3600 {
        formatter.allowedUnits = [.hour, .minute, .second]
      } else {
        formatter.allowedUnits = [.minute, .second]
      }

      return formatter.string(from: duration) ?? ""
    }
    
    static func getVideoThumbnail(from url: URL) -> UIImage {
        let asset = AVAsset(url: url)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let timestamp = CMTime.zero//CMTime(seconds: 0, preferredTimescale: 60)
        let thumbnail: UIImage?
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            thumbnail = UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            thumbnail = nil
            print("Image generation failed with error \(error)")
        }
        return thumbnail ?? UIImage.init()
    }
    
    static func getFitFrameSize(maxSize: CGSize, actualSize: CGSize)  -> CGSize {
        let maxWidth = maxSize.width
        let maxHeight = maxSize.height
        let actualWidth = actualSize.width
        let actualHeight = actualSize.height
        
        let widthRatio = maxWidth / actualWidth
        let heightRatio = maxHeight / actualHeight
        let scale = min(widthRatio, heightRatio)
        let imageWidth = scale * actualWidth
        let imageHeight = scale * actualHeight
        
        return CGSize(width: imageWidth, height: imageHeight)
    }
    
    static func cropImageToFitContentImage(bgImage: UIImage, contentImageSize: CGSize) -> UIImage? {
        var backgroundImage = bgImage
        let bgAspectRatio = backgroundImage.size.width / backgroundImage.size.height
        let contentAspectRatio = contentImageSize.width / contentImageSize.height
        var drawRect: CGRect = .zero
        var cropWidth: Bool = false
        var cropHeight: Bool = false
        
        if contentImageSize.width >= contentImageSize.height { // Landscape/Square
            if bgAspectRatio <= contentAspectRatio {
                // Fit width & crop height
                let newHeight = contentImageSize.width / bgAspectRatio
                
                cropHeight = true
                drawRect = CGRect(x: 0.0, y: 0.0, width: contentImageSize.width, height: newHeight)
                
            } else if bgAspectRatio > contentAspectRatio {
                // Fit height & crop width
                let newWidth = contentImageSize.height * bgAspectRatio
                
                cropWidth = true
                drawRect = CGRect(x: 0.0, y: 0.0, width: newWidth, height: contentImageSize.height)
            }
            
        } else { // Portrait
            if bgAspectRatio <= contentAspectRatio {
                // Fit width & crop height
                let newHeight = contentImageSize.width / bgAspectRatio
                
                cropHeight = true
                drawRect = CGRect(x: 0.0, y: 0.0, width: contentImageSize.width, height: newHeight)
                
            } else if bgAspectRatio > contentAspectRatio {
                // Fit height & crop width
                let newWidth = contentImageSize.height * bgAspectRatio
                
                cropWidth = true
                drawRect = CGRect(x: 0.0, y: 0.0, width: newWidth, height: contentImageSize.height)
            }
        }
        
        // Scaling image
        UIGraphicsBeginImageContextWithOptions(drawRect.size, false, 0.0)
        backgroundImage.draw(in: drawRect)
        backgroundImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        var originX: CGFloat = 0.0
        var originY: CGFloat = 0.0
        
        if cropWidth {
            originX = (backgroundImage.size.width - contentImageSize.width) / 2.0
        } else if cropHeight {
            originY = (backgroundImage.size.height - contentImageSize.height) / 2.0
        }
        
        // Cropping Image
        drawRect = CGRect(x: -originX, y: -originY, width: contentImageSize.width, height: contentImageSize.height)
        UIGraphicsBeginImageContextWithOptions(drawRect.size, false, 0.0)
        backgroundImage.draw(at: drawRect.origin)
        backgroundImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return backgroundImage
    }
    
    static func scaleImage(image: UIImage, toSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        let result: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return result
    }
    
    static func cropToSquareBounds(image: UIImage) -> UIImage {
        let contextSize = image.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth = contextSize.width
        var cgheight = contextSize.height
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        return Utils.croppingimageByImageName(image: image, toRect: rect)!
    }
    
    static func croppingimageByImageName(image: UIImage, toRect: CGRect) -> UIImage? {
        let rad: (Double) -> CGFloat = { deg in
            return CGFloat(deg / 180.0 * .pi)
        }
        var rectTransform: CGAffineTransform
        switch image.imageOrientation {
        case .left:
            let rotation = CGAffineTransform(rotationAngle: rad(90))
            rectTransform = rotation.translatedBy(x: 0, y: -image.size.height)
        case .right:
            let rotation = CGAffineTransform(rotationAngle: rad(-90))
            rectTransform = rotation.translatedBy(x: -image.size.width, y: 0)
        case .down:
            let rotation = CGAffineTransform(rotationAngle: rad(-180))
            rectTransform = rotation.translatedBy(x: image.size.width, y: -image.size.height)
        default:
            rectTransform = .identity
        }
        
        rectTransform = rectTransform.scaledBy(x: image.scale, y: image.scale)
        
        let transformedRect = toRect.applying(rectTransform)
        let cgImage         = image.cgImage
        let croppedCGImage  = cgImage!.cropping(to: transformedRect)!
        let image           = UIImage(cgImage: croppedCGImage, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    static func addImageToImage(img: UIImage, withImage2 img2: UIImage, andRect cropRect: CGRect) -> UIImage {
        let size: CGSize = CGSize(width: img.size.width, height: img.size.height)
        let window: UIWindow = UIApplication.shared.windows.first!
        
        let scale: CGFloat = window.screen.scale
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let pointImg1: CGPoint = CGPoint.zero
        img.draw(at: pointImg1)
        
        let pointImg2: CGPoint = cropRect.origin;
        img2.draw(at: pointImg2)
        
        let result = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return result
    }
    
    static func roundedRectImageFromImage(image: UIImage?, withRadious radius: CGFloat) -> UIImage {
        if (radius == 0.0) { return image! }
        
        if (image != nil) {
            let imageWidth: CGFloat  = image!.size.width
            let imageHeight: CGFloat = image!.size.height
            
            let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: imageWidth, height: imageHeight)
            let window: UIWindow = UIApplication.shared.windows.first!
            
            let scale: CGFloat = window.screen.scale
            UIGraphicsBeginImageContextWithOptions(rect.size, false, scale);
            
            let  context: CGContext = UIGraphicsGetCurrentContext()!
            
            context.beginPath()
            context.saveGState()
            context.translateBy(x: rect.minX, y: rect.minY)
            context.scaleBy(x: radius, y: radius)
            
            let rectWidth: CGFloat = rect.width / radius
            let rectHeight: CGFloat = rect.height / radius
            
            context.move(to: CGPoint(x: rectWidth, y: rectHeight/2.0))
            context.addArc(tangent1End: CGPoint(x: rectWidth, y: rectHeight),
                           tangent2End: CGPoint(x: rectWidth/2.0, y: rectHeight), radius: radius)
            context.addArc(tangent1End: CGPoint(x: 0.0, y: rectHeight),
                           tangent2End: CGPoint(x: 0.0, y: rectHeight/2.0), radius: radius)
            context.addArc(tangent1End: CGPoint(x: 0.0, y: 0.0),
                           tangent2End: CGPoint(x: rectWidth/2.0, y: 0.0), radius: radius)
            context.addArc(tangent1End: CGPoint(x: rectWidth, y: 0.0),
                           tangent2End: CGPoint(x: rectWidth, y: rectHeight/2.0), radius: radius)
            context.restoreGState()
            context.closePath()
            context.clip()
            
            image?.draw(in: CGRect(x: 0.0, y: 0.0, width: imageWidth, height: imageHeight))
            
            let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return newImage
        }
        
        return image!
    }
    
    static func getColor(red: Int, green: Int, blue: Int, alpha: Int = 255) -> CIColor {
        return CIColor(
            red: CGFloat(Double(red) / 255.0),
            green: CGFloat(Double(green) / 255.0),
            blue: CGFloat(Double(blue) / 255.0),
            alpha: CGFloat(Double(alpha) / 255.0))
    }
    
    static func getColorImage(red: Int, green: Int, blue: Int, alpha: Int = 255, rect: CGRect) -> CIImage {
        let color = self.getColor(red: red, green: green, blue: blue, alpha: alpha)
        return CIImage(color: color).cropped(to: rect)
    }
    
    static func convertToColorImage(color: CIColor, rect: CGRect) -> CIImage {
        return CIImage(color: color).cropped(to: rect)
    }
    
    static func applyBlurEffectOnImage(image: UIImage, blurRadiusInPixels: Float, type: BPBlurEffect) -> UIImage {
        var filteredImage = image
        
        if type == .blur {
//            let blur = GaussianBlur()
//            blur.blurRadiusInPixels = blurRadiusInPixels//4.0
//            filteredImage = image.filterWithOperation(blur)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        } else if type == .motion {
//            let blur = MotionBlur()
//            blur.blurSize = 4.0
//            filteredImage = image.filterWithOperation(blur)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
            
        } else if type == .zoom {
//            let blur = ZoomBlur()
//            blur.blurSize = 4.0
//            filteredImage = image.filterWithOperation(blur)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
            
        } else if type == .pixel {
//            let pixellate = Pixellate()
//            pixellate.fractionalWidthOfAPixel =  0.05
//            filteredImage = image.filterWithOperation(pixellate)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
            
        } else if type == .crystallize {
            filteredImage = filterWithCrystallize(radius: 20.0, origImg: image)
        }  else if type == .hexagon {
            filteredImage = filterWithHexagon(scale: 8.0, origImg: image)
        } else if type == .pointilize {
            filteredImage = filterWithPointilize(radius: 10.0, origImg: image)
        } else if type == .gloom {
            filteredImage = filterWithGloom(radius: 10.0, origImg: image)
        } else if type == .noir {
            filteredImage = filterWithNoir(origImg: image)
        } else if type == .vibrant {
//            let filter = Vibrance()
//            filter.vibrance = 1.2
//            filteredImage = image.filterWithOperation(filter)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
            
        } else if type == .sepia {
//            let filter = SepiaToneFilter()
//            filter.intensity = 1.0
//            filteredImage = image.filterWithOperation(filter)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
            
        } else if type == .mono {
            filteredImage = filterWithMono(origImg: image)
        } else if type == .e1977 {
            filteredImage = filterWith1977(origImg: image)
        } else if type == .clarendon {
            filteredImage = filterWithClarendon(origImg: image)
        } else if type == .nashville {
            filteredImage = filterWithNashville(origImg: image)
        } else if type == .toaster {
            filteredImage = filterWithToaster(origImg: image)
            
        } else if type == .haze {
//            let filter = Haze()
//            filter.distance = 0.2
//            filteredImage = image.filterWithOperation(filter)
//            filteredImage = UIImage(cgImage: filteredImage.cgImage!, scale: image.scale, orientation: image.imageOrientation)
        }
        
        return filteredImage
    }
    
    static func applyFilterOnImage(image: NSObject, type: BPFilter, isVideoProcessing: Bool) -> NSObject {
        var filteredImage = image
        
        if type == .falsecolor1 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       andColor2: CIColor(red:0.0, green:0.0, blue:1.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor2 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.0, green:0.0, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor3 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.0, green:1.0, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor4 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.0, green:1.0, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor(red:0.0, green:0.0, blue:1.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor5 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:1.0, green:0.0, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor.green,
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor6 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:1.0, green:1.0, blue:0.0, alpha:1.0),
                                                       andColor2: CIColor.blue,
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor7 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:1.0, green:0.5, blue:0.5, alpha:1.0),
                                                       andColor2: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor8 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:1.0, green:0.5, blue:0.0, alpha:1.0),
                                                       andColor2: CIColor.blue,
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor9 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.5, green:1.0, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor.blue,
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor10 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.5, green:0.5, blue:1.0, alpha:1.0),
                                                       andColor2: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .falsecolor11 {
            filteredImage = Utils.filterWithFalseColor(imgObject: image,
                                                       withColor1: CIColor(red:0.5, green:1.0, blue:0.5, alpha:1.0),
                                                       andColor2: CIColor(red:1.0, green:0.0, blue:0.0, alpha:1.0),
                                                       isVideoProcessing: isVideoProcessing)
        } else if type == .hue1 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((-3.14) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .hue2 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((-1.7) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .hue3 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((-0.7) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .hue4 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((1.0) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .hue5 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((1.75) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .hue6 {
            filteredImage = Utils.filterWithHueAdjust(imgObject: image, angle: CGFloat((2.5) * (180/Double.pi)), isVideoProcessing: isVideoProcessing)
        } else if type == .chrome {
            filteredImage = filterWithPhotoChrome(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .fade {
            filteredImage = filterWithPhotoEffectFade(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .instant {
            filteredImage = filterWithPhotoEffectInstant(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .process {
            filteredImage = filterWithPhotoEffectProcess(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .transfer {
            filteredImage = filterWithPhotoEffectTransfer(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .tonal {
            filteredImage = filterWithPhotoEffectTonal(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .colormonochrome {
            filteredImage = filterWithColorMonochrome(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .linescreen {
            filteredImage = filterWithLineScreen(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .comic {
            filteredImage = filterWithComicEffect(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .cmykhalftone {
            filteredImage = filterWithCMYKHalftone(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .maskalpha {
            filteredImage = filterWithMaskToAlpha(imgObject: image, isVideoProcessing: isVideoProcessing)
        } else if type == .dotscreen {
            filteredImage = filterWithDotScreen(angle: 0.0, width: 25.0, sharpness: 0.70, imgObject: image, isVideoProcessing: isVideoProcessing)
        }
        
        return filteredImage
    }
    
    static func filterWithDiscBlur(radius: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur, kCIInputRadiusKey: radius]
        let filter: CIFilter = CIFilter(name: "CIDiscBlur", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithGaussianBlur(radius: Float, imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }

        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img, kCIInputRadiusKey: radius]
            let filter: CIFilter = CIFilter(name: "CIGaussianBlur", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func convertToUIImage(origImg: CIImage, scale: CGFloat, imageOrientation: UIImage.Orientation) -> UIImage {
        let context = CIContext(options: nil)
        let cgImage = context.createCGImage(origImg, from: origImg.extent)!
        let uiOutImg = UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithNoir(origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur]
        let filter: CIFilter = CIFilter(name: "CIPhotoEffectNoir", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(output, from: output.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithFalseColor(imgObject: NSObject, withColor1 color0: CIColor, andColor2 color1: CIColor,
                                     isVideoProcessing: Bool) -> NSObject
    {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }

        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img,
                                                       "inputColor0": color0,
                                                       "inputColor1": color1]
            let filter: CIFilter = CIFilter(name: "CIFalseColor", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
                    
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                        
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithHueAdjust(imgObject: NSObject, angle: CGFloat, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img,
                                               kCIInputAngleKey: angle]
            let filter: CIFilter = CIFilter(name: "CIHueAdjust", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithPhotoChrome(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectChrome", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithPhotoEffectFade(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectFade", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
            
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithPhotoEffectInstant(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectInstant", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithPhotoEffectProcess(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectProcess", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithPhotoEffectTransfer(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectTransfer", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithGloom(radius: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur, kCIInputRadiusKey: radius, kCIInputIntensityKey: 1.0]
        let filter: CIFilter = CIFilter(name: "CIGloom", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithMono(origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur]
        let filter: CIFilter = CIFilter(name: "CIPhotoEffectMono", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithPhotoEffectTonal(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectTonal", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWith1977(origImg: UIImage) -> UIImage {
        let ciImage: CIImage = CIImage(cgImage: origImg.cgImage!)
        let filterImage = Utils.getColorImage(red: 243, green: 106, blue: 188, alpha: Int(255 * 0.15), rect: ciImage.extent)
        let backgroundImage = ciImage
            .applyingFilter("CIColorControls", parameters: [
                "inputSaturation": 1.3,
                "inputBrightness": 0.1,
                "inputContrast": 1.20,
                ])
            .applyingFilter("CIHueAdjust", parameters: [
                "inputAngle": 0.3,
                ])
        let output: CIImage = filterImage
            .applyingFilter("CIScreenBlendMode", parameters: [
                "inputBackgroundImage": backgroundImage,
                ])
            .applyingFilter("CIToneCurve", parameters: [
                "inputPoint0": CIVector(x: 0, y: 0),
                "inputPoint1": CIVector(x: 0.25, y: 0.20),
                "inputPoint2": CIVector(x: 0.5, y: 0.5),
                "inputPoint3": CIVector(x: 0.75, y: 0.80),
                "inputPoint4": CIVector(x: 1, y: 1),
                ])
        let croppedImage: CIImage = output.cropped(to: ciImage.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithClarendon(origImg: UIImage) -> UIImage {
        let foregroundImage: CIImage = CIImage(cgImage: origImg.cgImage!)
        let backgroundImage = Utils.getColorImage(
            red: 127, green: 187, blue: 227, alpha: Int(255 * 0.2), rect: foregroundImage.extent)
        let output: CIImage = foregroundImage
            .applyingFilter("CIOverlayBlendMode", parameters: [
                "inputBackgroundImage": backgroundImage,
                ])
            .applyingFilter("CIColorControls", parameters: [
                "inputSaturation": 1.35,
                "inputBrightness": 0.05,
                "inputContrast": 1.1,
                ])
        let croppedImage: CIImage = output.cropped(to: foregroundImage.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    
    static func filterWithNashville(origImg: UIImage) -> UIImage {
        let foregroundImage: CIImage = CIImage(cgImage: origImg.cgImage!)
        let backgroundImage = Utils.getColorImage(
            red: 247, green: 176, blue: 153, alpha: Int(255 * 0.56), rect: foregroundImage.extent)
        let backgroundImage2 = Utils.getColorImage(
            red: 0, green: 70, blue: 150, alpha: Int(255 * 0.4), rect: foregroundImage.extent)
        let output: CIImage = foregroundImage
            .applyingFilter("CIDarkenBlendMode", parameters: [
                "inputBackgroundImage": backgroundImage,
                ])
            .applyingFilter("CISepiaTone", parameters: [
                "inputIntensity": 0.2,
                ])
            .applyingFilter("CIColorControls", parameters: [
                "inputSaturation": 1.2,
                "inputBrightness": 0.05,
                "inputContrast": 1.1,
                ])
            .applyingFilter("CILightenBlendMode", parameters: [
                "inputBackgroundImage": backgroundImage2,
                ])
        let croppedImage: CIImage = output.cropped(to: foregroundImage.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithToaster(origImg: UIImage) -> UIImage {
        let ciImage: CIImage = CIImage(cgImage: origImg.cgImage!)
        let width = ciImage.extent.width
        let height = ciImage.extent.height
        let centerWidth = width / 2.0
        let centerHeight = height / 2.0
        let radius0 = min(width / 4.0, height / 4.0)
        let radius1 = min(width / 1.5, height / 1.5)
        print(width, height, centerWidth, centerHeight, radius0, radius1)
        
        let color0 = Utils.getColor(red: 128, green: 78, blue: 15, alpha: 255)
        let color1 = Utils.getColor(red: 79, green: 0, blue: 79, alpha: 255)
        let circle = CIFilter(name: "CIRadialGradient", parameters: [
            "inputCenter": CIVector(x: centerWidth, y: centerHeight),
            "inputRadius0": radius0,
            "inputRadius1": radius1,
            "inputColor0": color0,
            "inputColor1": color1,
            ])?.outputImage?.cropped(to: ciImage.extent)
        
        let output: CIImage = ciImage
            .applyingFilter("CIColorControls", parameters: [
                "inputSaturation": 1.0,
                "inputBrightness": 0.01,
                "inputContrast": 1.1,
                ])
            .applyingFilter("CIScreenBlendMode", parameters: [
                "inputBackgroundImage": circle!,
                ])
        let croppedImage: CIImage = output.cropped(to: ciImage.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithCrystallize(radius: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur, kCIInputRadiusKey: radius]
        let filter: CIFilter = CIFilter(name: "CICrystallize", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithColorMonochrome(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIColorMonochrome", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithMaskToAlpha(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIMaskToAlpha", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithComicEffect(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img]
            let filter: CIFilter = CIFilter(name: "CIComicEffect", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithCMYKHalftone(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img,
                                               kCIInputAngleKey: 0.0,
                                               kCIInputWidthKey: 35,
                                               kCIInputSharpnessKey: 0.50,
                                               "inputGCR": 0.0,
                                               "inputUCR": 0.50]
            
            let filter: CIFilter = CIFilter(name: "CICMYKHalftone", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithLineScreen(imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img,
                                               kCIInputAngleKey: 0.0,
                                               kCIInputWidthKey: 20.0,
                                               kCIInputSharpnessKey: 0.70]
            let filter: CIFilter = CIFilter(name: "CILineScreen", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            let croppedImage: CIImage = output.cropped(to: img.extent)
            
            if isVideoProcessing {
                return croppedImage
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithHexagon(scale: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur, kCIInputScaleKey: scale]
        let filter: CIFilter = CIFilter(name: "CIHexagonalPixellate", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithPointilize(radius: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur, kCIInputRadiusKey: radius]
        let filter: CIFilter = CIFilter(name: "CIPointillize", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(croppedImage, from: croppedImage.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func filterWithDotScreen(angle: Float, width: Float, sharpness: Float, imgObject: NSObject, isVideoProcessing: Bool) -> NSObject {
        var imageToBlur: CIImage? = nil
        var origImg: UIImage? = nil
        
        if isVideoProcessing {
            imageToBlur = imgObject as? CIImage
        } else {
            origImg = imgObject as? UIImage
            imageToBlur = CIImage(cgImage: (origImg?.cgImage)!)
        }
        
        if let img = imageToBlur {
            let parameters: [String : Any]? = [kCIInputImageKey: img,
                                               kCIInputAngleKey: angle,
                                               kCIInputWidthKey: width,
                                               kCIInputSharpnessKey: sharpness]
            let filter: CIFilter = CIFilter(name: "CIDotScreen", parameters: parameters)!
            let output: CIImage = filter.outputImage!
            //let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
            
            if isVideoProcessing {
                return output
            }
            
            if let uiimg = origImg {
                let context = CIContext(options: nil)
                let cgImage: CGImage = context.createCGImage(output, from: output.extent)!
                let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: uiimg.scale, orientation: uiimg.imageOrientation)
                
                return uiOutImg
            }
        }
        
        return imgObject
    }
    
    static func filterWithDepthOfField(radius: Float, saturation: Float, intensity: Float, origImg: UIImage) -> UIImage {
        let imageToBlur: CIImage = CIImage(cgImage: origImg.cgImage!)
        let parameters: [String : Any]? = [kCIInputImageKey: imageToBlur,
                                           kCIInputRadiusKey: radius,
                                           kCIInputSaturationKey: saturation,
                                           "inputUnsharpMaskIntensity": intensity]
        let filter: CIFilter = CIFilter(name: "CIDepthOfField", parameters: parameters)!
        let output: CIImage = filter.outputImage!
        //let croppedImage: CIImage = output.cropped(to: imageToBlur.extent)
        
        let context = CIContext(options: nil)
        let cgImage: CGImage = context.createCGImage(output, from: output.extent)!
        let uiOutImg: UIImage = UIImage(cgImage: cgImage, scale: origImg.scale, orientation: origImg.imageOrientation)
        
        return uiOutImg
    }
    
    static func checkIfInstaFilter(type: BPFilter) -> Bool {
        if type == .amaro || type == .brannon || type == .earlybird ||
            type == .hudson || type == .inkwell || type == .lomo ||
            type == .lordKelvin || type == .rise || type == .sierra ||
            type == .sutro || type == .valencia || type == .walden ||
            type == .xproll
        {
            return true
        }
    
        return false
    }
    
    static func getAssetThumbnail(asset: PHAsset, handler: (@escaping (UIImage?, [AnyHashable : Any]?) -> Swift.Void) ) {
        var cellDimension = UIScreen.main.bounds.size.width / 4.0
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            cellDimension = UIScreen.main.bounds.size.width / 8.0
        }
        
        let size = CGSize(width: cellDimension, height: cellDimension)
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        
        option.isNetworkAccessAllowed = true
        option.isSynchronous = false
        option.deliveryMode = .highQualityFormat
        
        manager.requestImage(for: asset,
                             targetSize: size,
                             contentMode: .aspectFit,
                             options: option,
                             resultHandler: handler)
    }
    
    static func getAssetWithSize(asset: PHAsset, size: CGSize,
                                 handler: (@escaping (UIImage?, [AnyHashable : Any]?) -> Swift.Void))
    {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        
        option.isNetworkAccessAllowed = true
        option.isSynchronous = false
        option.deliveryMode = .highQualityFormat
        
        manager.requestImage(for: asset,
                             targetSize: size,
                             contentMode: .aspectFit,
                             options: option,
                             resultHandler: handler)
    }
    
    static func getRectForAspectRatio(ratio: String) -> CGRect {
        var drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        
        if ratio.elementsEqual("Square") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        } else if ratio.elementsEqual("3:4") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 22.5, height: 30.0)
        } else if ratio.elementsEqual("4:3") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 22.5)
        } else if ratio.elementsEqual("6:4") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 20.0)
        } else if ratio.elementsEqual("4:6") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 30.0)
        } else if ratio.elementsEqual("7:6") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 25.7)
        } else if ratio.elementsEqual("6:7") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 25.7, height: 30.0)
        } else if ratio.elementsEqual("10:8") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 24.0)
        } else if ratio.elementsEqual("8:10") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 24.0, height: 30.0)
        } else if ratio.elementsEqual("16:9") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 16.875)
        } else if ratio.elementsEqual("9:16") {
            drawRect = CGRect(x: 0.0, y: 0.0, width: 16.875, height: 30.0)
        }
        
        return drawRect
    }
    
    static func drawRectangle(rect: CGRect, strokeColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        let bezier = UIBezierPath(rect: rect)
        
        context?.setStrokeColor(color.cgColor)
        
        bezier.lineWidth = 3.0
        bezier.lineJoinStyle = .bevel
        bezier.stroke()

        context!.addPath(bezier.cgPath)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();

        return image!
    }
    
    static func showAlert(title: String, message: String, controller: UIViewController) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
        }
        
        alertController.addAction(okAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func ipadMultiplier() -> CGFloat {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ) { return 2.0 }
        return 1.0;
    }
    
    static func isIpad() -> Bool {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
    }
    
    static func centerTextInTextView(textView tv: UITextView) {
        var topCorrect  = (tv.bounds.size.height - tv.contentSize.height * tv.zoomScale)/2.0;
        var leftCorrect = (tv.bounds.size.width  - tv.contentSize.width * tv.zoomScale)/2.0;
        
        topCorrect  = ( topCorrect  < 0.0 ? 0.0 : topCorrect );
        leftCorrect = ( leftCorrect < 0.0 ? 0.0 : leftCorrect );
        
        tv.contentOffset = CGPoint(x: -leftCorrect, y: -topCorrect)
    }
    
    static func updateTextView(textView viewBeingUpdated: UITextView, withTextView viewBeingUpdatedFrom: UITextView,
                               delegate: UITextViewDelegate)
    {
        viewBeingUpdated.backgroundColor = viewBeingUpdatedFrom.backgroundColor
        viewBeingUpdated.textAlignment = viewBeingUpdatedFrom.textAlignment
        viewBeingUpdated.delegate = delegate
        viewBeingUpdated.returnKeyType = UIReturnKeyType.default
        viewBeingUpdated.textColor = viewBeingUpdatedFrom.textColor;
        viewBeingUpdated.font = viewBeingUpdatedFrom.font;
        viewBeingUpdated.alpha = viewBeingUpdatedFrom.alpha;
        viewBeingUpdated.attributedText = viewBeingUpdatedFrom.attributedText;
    }    
    
    static func isPremiumUser() -> Bool {
        return UserDefaults.standard.bool(forKey: "is_premium_purchased")
    }
    
    static func cropByRemovingTransparentArea(image: UIImage) -> UIImage {
        let cgImage = image.cgImage!
        
        let width = cgImage.width
        let height = cgImage.height
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel:Int = 4
        let bytesPerRow = bytesPerPixel * width
        let bitsPerComponent = 8
        let bitmapInfo: UInt32 = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Big.rawValue
        
        guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo),
            let ptr = context.data?.assumingMemoryBound(to: UInt8.self) else {
                return image
        }
        
        context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: width, height: height))
        
        var minX = width
        var minY = height
        var maxX: Int = 0
        var maxY: Int = 0
        
        for x in 1 ..< width {
            for y in 1 ..< height {
                
                let i = bytesPerRow * Int(y) + bytesPerPixel * Int(x)
                let a = CGFloat(ptr[i + 3]) / 255.0
                
                if(a>0) {
                    if (x < minX) { minX = x };
                    if (x > maxX) { maxX = x };
                    if (y < minY) { minY = y};
                    if (y > maxY) { maxY = y};
                }
            }
        }
        
        let rect = CGRect(x: CGFloat(minX),y: CGFloat(minY), width: CGFloat(maxX-minX), height: CGFloat(maxY-minY))
        let imageScale:CGFloat = image.scale
        let croppedImage =  image.cgImage!.cropping(to: rect)!
        let ret = UIImage(cgImage: croppedImage, scale: imageScale, orientation: image.imageOrientation)
        
        return ret;
    }
    
    static func getStoryboardFilename() -> String? {
        var storyboard: String? = nil
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        let limitValue4Inch = CGFloat(568.0)
        let limitValueX = CGFloat(812.0)
        let limitValueXR = CGFloat(896.0)
        var orientedDimension = height
        
        // Check if Landscape
        if (width > height) {
            orientedDimension = width
        }
        
        if (orientedDimension > limitValue4Inch) {
            if (orientedDimension == limitValueX || orientedDimension == limitValueXR) {//iPhoneX,XR
                storyboard = "Main_iPhoneX"
            } else {    // greater than 4inch Display
                storyboard = "Main"
            }
        } else {    // under 4inch Display
            storyboard = "Main_4inch"
        }
        
        return storyboard
    }
    
    static func grabStoryboard() -> UIStoryboard? {
        var storyboard: UIStoryboard? = nil
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        let limitValue4Inch = CGFloat(568.0)
        let limitValueX = CGFloat(812.0)
        let limitValueXR = CGFloat(896.0)
        var orientedDimension = height
        
        // Check if Landscape
        if (width > height) {
            orientedDimension = width
        }
        
        if (orientedDimension > limitValue4Inch) {
            if (orientedDimension == limitValueX || orientedDimension == limitValueXR) {//iPhoneX,XR
                storyboard = UIStoryboard(name: "Main_iPhoneX", bundle: nil)
            } else {    // greater than 4inch Display
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }
        } else {    // under 4inch Display
            storyboard = UIStoryboard(name: "Main_4inch", bundle: nil)
        }
        
        return storyboard
    }
    
    static func isIPhoneXDevice() -> Bool {
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        let limitValueX = CGFloat(812.0)
        let limitValueXR = CGFloat(896.0)
        var orientedDimension = height
        var isIPhoneXDevice = false
        
        // Check if Landscape
        if (width > height) {
            orientedDimension = width;
        }
        
        if (orientedDimension == limitValueX || orientedDimension == limitValueXR) {    // iPhone X
            isIPhoneXDevice = true
        }
        
        return isIPhoneXDevice
    }
}
