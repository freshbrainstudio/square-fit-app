//
//  Constants.swift
//  BlurPhoto
//
//  Created by Qaiser Butt on 3/22/18.
//  Copyright © 2018 Red Owl. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = CGFloat.maximum(SCREEN_WIDTH, SCREEN_HEIGHT)
    static let IS_IPHONE_X = (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
    static let IS_IPHONE_UNDER_4INCH = (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)
    static let UIColorFromRGB: (CGFloat, CGFloat, CGFloat) -> UIColor = { r,g,b  in
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    static let fonts: [String] = ["Asimov", "Aspire-DemiBold", "BreveSC", "BroshK", "Bungasai", "CaviarDreams", "CroissantOne-Regular", "JennaSue", "Kirvy-Bold", "Limelight", "Lobster1.4", "Monument_Valley_1.2-Regular", "Namskout", "NORTHWEST-Bold", "Parisish", "PoiretOne-Regular", "RegencieLight", "Rochester-Regular", "Rudiment", "Snickles", "TimeBurner", "Trench-Thin", "VarianeScript", "WeAreInLove"];
    
    static let allTextColors: [UIColor] =
        [UIColorFromRGB(255,255,255), UIColorFromRGB(234,237,228), UIColorFromRGB(255,249,234), UIColorFromRGB(254,235,228),
         UIColorFromRGB(229,230,250), UIColorFromRGB(220,239,255), UIColorFromRGB(180,223,224), UIColorFromRGB(214,235,229),
         UIColorFromRGB(243,228,196), UIColorFromRGB(247,213,211), UIColorFromRGB(249,186,195), UIColorFromRGB(160,208,230),
         UIColorFromRGB(208,192,215), UIColorFromRGB(236,180,163), UIColorFromRGB(236,184,126), UIColorFromRGB(237,173,200),
         UIColorFromRGB(0,0,0),
         //Page 2
            UIColorFromRGB(200,213,219),UIColorFromRGB(236,227,218),UIColorFromRGB(206,181,170),UIColorFromRGB(236,237,202),
            UIColorFromRGB(213,210,193),UIColorFromRGB(168,156,141),UIColorFromRGB(126,127,147),UIColorFromRGB(148,119,115),
            UIColorFromRGB(209,195,188),UIColorFromRGB(194,180,111),UIColorFromRGB(195,197,180),UIColorFromRGB(63,90,107),
            UIColorFromRGB(58,47,44),   UIColorFromRGB(111,65,75),  UIColorFromRGB(152,81,90),  UIColorFromRGB(137,137,111),
            UIColorFromRGB(92,103,89),
            // Page3
            UIColorFromRGB(218,224,237),UIColorFromRGB(208,212,206),UIColorFromRGB(223,209,183),UIColorFromRGB(247,234,218),
            UIColorFromRGB(225,204,209),UIColorFromRGB(221,184,184),UIColorFromRGB(175,192,209),UIColorFromRGB(182,196,179),
            UIColorFromRGB(197,184,168),UIColorFromRGB(245,205,193),UIColorFromRGB(216,185,213),UIColorFromRGB(77,106,146),
            UIColorFromRGB(136,149,166),UIColorFromRGB(125,137,113),UIColorFromRGB(175,148,131),UIColorFromRGB(202,150,136),
            UIColorFromRGB(147,65,101)]
    
    static let effects: [BPBlurEffect] = [.blur, .zoom, .motion, .pixel, .crystallize, .hexagon, .pointilize, .gloom, .noir, .vibrant,
                                          .sepia, .mono, .e1977, .clarendon, .nashville, .toaster, .haze]
    
    static let bpFilters: [BPFilter] = [/*.amaro, .brannon, .earlybird, .hudson, .inkwell, .lomo, .lordKelvin, .rise, .sierra, .sutro,
                                        .valencia, .walden, .xproll, */.hue1, .hue2, .hue3, .hue4, .hue5, .hue6, .chrome, .fade, .instant,
                                        .process, .transfer, .dotscreen, .tonal, .colormonochrome, .maskalpha, .linescreen, .comic, .cmykhalftone,
                                        .falsecolor1, .falsecolor2, .falsecolor3, .falsecolor4, .falsecolor5, .falsecolor6, .falsecolor7,
                                        .falsecolor8, .falsecolor9, .falsecolor10, .falsecolor11]
    
    /*"Blur", "Zoom", "Motion", "Pixel", "Crystal", "Hexagon", "Points", "Gloom", "Noir", "Vibrant",
     "Sepia", "Mono", "1977", "Clarendon", "Nashville", "Toaster", "Haze",*/
    
    static let effectNames: [String] = ["e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "e10","e11", "e12", "e13", "e14", "e15", "e16", "e17","Amaro", "Brannon", "Earlybird","Hudson", "Inkwell", "Lomo", "LordKelvin", "Rise", "Sierra", "Sutro", "Valencia", "Walden", "Xproll","HueAdjust", "HueAdjust", "HueAdjust", "HueAdjust", "HueAdjust", "HueAdjust", "Chrome", "Fade","Instant", "Process", "Transfer", "Dot Screen", "Tonal", "Monochrome", "MaskToAlpha", "LineScreen","Comic", "HalfTone", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor","FalseColor", "FalseColor", "FalseColor", "FalseColor"]
    
    static let bpOriginalFilterNames: [String] = ["Amaro", "Brannon", "Earlybird", "Hudson", "Inkwell", "Lomo", "LordKelvin", "Rise", "Sierra","Sutro", "Valencia", "Walden", "Xproll", "HueAdjust", "HueAdjust", "HueAdjust", "HueAdjust","HueAdjust", "HueAdjust", "Chrome", "Fade", "Instant", "Process", "Transfer", "Dot Screen","Tonal", "Monochrome", "MaskToAlpha", "LineScreen", "Comic", "HalfTone", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor", "FalseColor","FalseColor", "FalseColor", "FalseColor", "FalseColor"]
    
    static let bpFilterNames: [String] = ["f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", "f14", "f15", "f16","f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25", "f26", "f27", "f28", "f29"]
}

enum BPBlurEffect: Int {
    case none = 0, blur, zoom, motion, pixel, crystallize, hexagon, pointilize, gloom, noir, vibrant, sepia, mono, e1977, clarendon, nashville, toaster, haze
}

enum BPFilter: Int {
    case none = 0,
    amaro, brannon, earlybird, hudson, inkwell, lomo, lordKelvin, rise, sierra, sutro, valencia, walden, xproll, // Insta Filters
    falsecolor1, falsecolor2, falsecolor3, falsecolor4, falsecolor5, falsecolor6, falsecolor7, falsecolor8, falsecolor9,
    falsecolor10, falsecolor11, hue1, hue2, hue3, hue4, hue5, hue6, chrome, fade, instant, process, transfer, tonal, colormonochrome,
    maskalpha, linescreen, comic, cmykhalftone, dotscreen
}

enum MediaType: Int {
    case photo = 0, video
}
