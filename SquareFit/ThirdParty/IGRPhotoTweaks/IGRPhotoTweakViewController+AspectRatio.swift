//
//  IGRPhotoTweakViewController+AspectRatio.swift
//  Pods
//
//  Created by Vitalii Parovishnyk on 4/26/17.
//
//

import Foundation
import UIKit

extension IGRPhotoTweakViewController {
    public func resetAspectRect() {
        UIView.animate(withDuration: kAnimationDuration, animations: {() -> Void in
            self.photoView.resetAspectRect()
        })
    }
    
    public func setCropAspectRect(aspect: String) {
        UIView.animate(withDuration: kAnimationDuration, animations: {() -> Void in
            self.photoView.setCropAspectRect(aspect: aspect)
        })
    }
    
    public func lockAspectRatio(_ lock: Bool) {
        self.photoView.lockAspectRatio(lock)
    }
    
}
